﻿using System.Globalization;
using System.IO;

namespace Mt.Proxaml.Application.Infrastructure
{
    public static class Globals
    {
        // language keys
        public const string ERRSCALE1 = nameof(ERRSCALE1);
        public const string MESSAGETITLE = nameof(MESSAGETITLE);
        public const string MESSAGETEXT = nameof(MESSAGETEXT);
        public const string BUTTONTEXT1 = nameof(BUTTONTEXT1);

        // ribbon ids

        public const string ZeroItemId = "Default.Zero";
        public const string ClearItemId = "Default.Clear";
        public const string TareItemId = "Default.Tare";
        public const string PrintItemId = "Default.Print";
        public const string PresetTareItemId = "Default.PresetTare";
        public const string SwitchScaleItemId = "Default.SwitchScale";
        public const string SwitchUnitItemId = "Default.SwitchUnit";
        public const string AlibiTableItemId = "Default.AlibiTable";
        public const string ShowHighResolutionItemId = "Default.ShowHighResolution";

        public const string PlaceHolderKeyId = "empty";

        public const string DeleteIcon = "Delete_Bin_MTBlue";
        public const string AddIcon = "Add_Plus_MTBlue";
        public const string EditIcon = "Edit_MTBlue";
        public const string CustomerIcon = "Search_MTBlue";

        // location where connection (DB) string information is stored
        private const string Directory = @"C:\Packages\Mt.Proxaml.Wpf\Data";
        private const string FileName = "ConnectionString.json";
        private static readonly string[] _paths = { Directory, FileName };
        public static readonly string PathToConnectionString = Path.Combine(_paths);

        // Culture info used for parsing of NetWeightString value which contains a . for decimal numbers
        public static readonly CultureInfo Culture = new CultureInfo("de-CH");

        // available images

        // Add_Plus.svg
        // Add_Plus_MTBlue.svg
        // Admin.svg
        // Admin_MTBlue.svg
        // Alibi.svg
        // Alibi_MTBlue.svg
        // Apps_Programlist.svg
        // Arrow_Down.svg
        // Arrow_left.svg
        // Arrow_leftB.svg
        // Arrow_leftW.svg
        // Arrow_left_DarkGray.svg
        // Arrow_left_MTBlue.svg
        // Arrow_left_White.svg
        // Arrow_right.svg
        // Arrow_rightB.svg
        // Arrow_rightW.svg
        // Arrow_right_Breadcrumb_Separator.svg
        // Arrow_right_DarkGray.svg
        // Arrow_right_MTBlue.svg
        // Arrow_right_TB.svg
        // Arrow_right_White.svg
        // Arrow_Up.svg
        // Article_Table.svg
        // Article_Table_MTBlue.svg
        // Asterisk.svg
        // Backspace.svg
        // Bargraph.svg
        // Brightness_bright.svg
        // Brightness_dark.svg
        // Brightness_default.svg
        // CalibrationTest.svg
        // CalibrationTest_MTBlue.svg
        // Cancel.svg
        // Cancel_MTBlue.svg
        // Caps.svg
        // CapsLock.svg
        // Class1.svg
        // Class1_MTBlue.svg
        // Class2.svg
        // Class2_MTBlue.svg
        // Class3.svg
        // Class3L.svg
        // Class3L_MTBlue.svg
        // Class3_MTBlue.svg
        // Class4.svg
        // Class4_MTBlue.svg
        // Clear.svg
        // Clear_Entry.svg
        // Clear_MTBlue.svg
        // Color_Themes.svg
        // Copy.svg
        // Copy_MTBlue.svg
        // Delete_Bin.svg
        // Delete_Bin_MTBlue.svg
        // Device.svg
        // DoubleArrow_left_DarkGray.svg
        // DoubleArrow_left_MTBlue.svg
        // DoubleArrow_left_White.svg
        // DoubleArrow_right_DarkGray.svg
        // DoubleArrow_right_MTBlue.svg
        // DoubleArrow_right_White.svg
        // Edit.svg
        // Edit_MTBlue.svg
        // Enter.svg
        // Error.svg
        // Exit_TB.svg
        // Export.svg
        // Export_MTBlue.svg
        // Eye.svg
        // Eye_MTBlue.svg
        // File.svg
        // Filter.svg
        // FilterOff.svg
        // FilterOff_MTBlue.svg
        // FilterOn.svg
        // FilterOn_MTBlue.svg
        // Forms-Header-Back-Arrow.svg
        // FullScreen.svg
        // FullScreen_TB.svg
        // GWP_OK_MTBlue.svg
        // GWP_X_Red.svg
        // Help.svg
        // Icon_Placeholder.svg
        // ID_Form.svg
        // ID_Form_MTBlue.svg
        // Import.svg
        // Import_MTBlue.svg
        // Info.svg
        // Info_TB.svg
        // ip.svg
        // Ip_TB.svg
        // ItemAppView.png
        // ItemInfo.png
        // ItemSettings.png
        // Language.svg
        // Language_MTBlue.svg
        // Left.svg
        // Link.svg
        // Logbook.svg
        // Logbook_MTBlue.svg
        // Login_Logout.svg
        // Login_Logout_TB.svg
        // Login_Logout_White.svg
        // Logout.svg
        // Logout_MTBlue.svg
        // Menu_Burger.svg
        // Menu_Burger_MTBlue.svg
        // Message.svg
        // Message_MTBlue.svg
        // Metrology_OK_Status.svg
        // Metrology_OK_Status_TB.svg
        // Metrology_OK_Status_White.svg
        // Metrology_Warning_Status.svg
        // Minus.svg
        // Minus_MTBlue.svg
        // min_Weigh_Error.svg
        // min_Weigh_Error_Red.svg
        // More_Details.svg
        // More_Details_MTBlue.svg
        // MT_Wordmark.svg
        // Namur_Failure_Red.svg
        // Namur_Function_check_Orange.svg
        // Namur_Maintenance_required_Blue.svg
        // Namur_Normal_Green.svg
        // Namur_Out_of_specifiation_Yellow.svg
        // noIP.svg
        // Ok_Check.svg
        // Ok_Check_DarkGray.svg
        // Ok_Check_MTBlue.svg
        // Ok_Check_White.svg
        // OK_Enter.svg
        // OpenSubMenuIcon.png
        // Operator.svg
        // Operator_MTBlue.svg
        // Paste.svg
        // Pause.svg
        // Play.svg
        // Play_MTBlue.svg
        // Power.svg
        // PresetTare.svg
        // PresetTare_MTBlue.svg
        // Print.svg
        // Print_MTBlue.svg
        // Reference.svg
        // Reference_MTBlue.svg
        // Reset.svg
        // Reset_MTBlue.svg
        // Result_Report.svg
        // Right.svg
        // Right_TB.svg
        // Scale.svg
        // Scale_MTBlue.svg
        // Scale_TB.svg
        // Search.svg
        // Search_MTBlue.svg
        // Service.svg
        // Settings.svg
        // Settings_TB.svg
        // Shift.svg
        // Shuffle_TB.svg
        // Small_screen.svg
        // SplashScreen.png
        // Stop.svg
        // Stop_MTBlue.svg
        // SubTotal.svg
        // Sum.svg
        // Sum_MTBlue.svg
        // Supervisor.svg
        // Supervisor_MTBlue.svg
        // SwitchScaleAsync.svg
        // SwitchScale_MTBlue.svg
        // SwitchUnit.svg
        // SwitchUnit_MTBlue.svg
        // Tare.svg
        // Tare_MTBlue.svg
        // Tare_Table.svg
        // Tare_Table_MTBlue.svg
        // Target.svg
        // Target_MTBlue.svg
        // ToggleWeightCount.svg
        // ToggleWeightCount_Inactive.svg
        // ToggleWeightCount_Inactive_MTBlue.svg
        // ToggleWeightCount_MTBlue.svg
        // Warning.svg
        // Weight.svg
        // Weight_Test_min.svg
        // Weight_Test_min_MTBlue.svg
        // X10.svg
        // X10_MTBlue.svg
        // Zero.svg
        // Zero_MTBlue.svg
    }
}