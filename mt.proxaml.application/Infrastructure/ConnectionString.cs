﻿namespace Mt.Proxaml.Application.Infrastructure
{
    public class ConnectionString
    {
        public string UserId { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }
        public string ServiceName { get; set; }
    }
}