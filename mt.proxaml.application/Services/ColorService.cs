﻿using System;
using System.Windows.Media;

namespace Mt.Proxaml.Application.Services
{
    public class ColorService : IColorService
    {
        private double _lowerLimit;
        private double _upperLimit;
        private Brush _color;

        public ColorService(IMeasurementService measurementService)
        {
            measurementService.MeasurementDone += ColorScreen;
            measurementService.BelowMinimumWeight += RemoveScreenColor;
        }

        public event Action<Brush> ColorChanged;

        public void UpdateParameters(double lowerLimit, double upperLimit)
        {
            _lowerLimit = lowerLimit;
            _upperLimit = upperLimit;
        }

        public void ColorScreen(double netWeight)
        {
            if (netWeight > _lowerLimit && netWeight < _upperLimit)
            {
                _color = Brushes.Green;
                ColorChanged?.Invoke(_color);
            }
            else
            {
                _color = Brushes.Red;
                ColorChanged?.Invoke(_color);
            }
        }

        public void RemoveScreenColor()
        {
            _color = Brushes.White;
            ColorChanged?.Invoke(_color);
        }
    }
}