﻿using Oracle.ManagedDataAccess.Client;

namespace Mt.Proxaml.Application.Services
{
    public static class TestConnectionStringService
    {
        public static void ConnectionStringIsValid(string testString)
        {
            using (OracleConnection conn = new OracleConnection(testString))
            {
                conn.Open();
            }
        }
    }
}