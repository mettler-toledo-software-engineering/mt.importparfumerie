﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text.Json;
using Mt.Proxaml.Application.Infrastructure;

namespace Mt.Proxaml.Application.Services
{
    public static class SerializationService
    {
        public static void ConfigurationToJson(ConnectionString connectionString)
        {
            string jsonString = JsonSerializer.Serialize(connectionString);
            string directory = Path.GetDirectoryName(Globals.PathToConnectionString);
            if (!Directory.Exists(directory))
            {
                if (directory == null)
                {
                    return;
                }

                Directory.CreateDirectory(directory);
            }

            File.WriteAllText(Globals.PathToConnectionString, jsonString);
        }

        public static string GetConnectionString()
        {
            try
            {
                ConnectionString connectionString = ConnectionStringFromJson();
                return ConnectionStringToString(connectionString);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                ConnectionString connectionString = new ConnectionString
                {
                    UserId = "UserId",
                    Password = "DataPassword",
                    Host = "DataHost",
                    Port = "DataPort",
                    ServiceName = "DataServiceName"
                };
                return ConnectionStringToString(connectionString);
            }
        }

        private static string ConnectionStringToString(ConnectionString connectionString)
        {
            return
                $"User Id={connectionString.UserId};Password={connectionString.Password};Data Source={connectionString.Host}:{connectionString.Port}/{connectionString.ServiceName}";
        }

        private static ConnectionString ConnectionStringFromJson()
        {
            string jsonString = File.ReadAllText(Globals.PathToConnectionString);
            ConnectionString readConnectionString = JsonSerializer.Deserialize<ConnectionString>(jsonString);
            return readConnectionString;
        }
    }
}