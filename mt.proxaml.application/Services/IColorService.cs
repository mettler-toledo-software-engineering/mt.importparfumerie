﻿using System;
using System.Windows.Media;

namespace Mt.Proxaml.Application.Services
{
    public interface IColorService
    {
        event Action<Brush> ColorChanged;
        void UpdateParameters(double lowerLimit, double upperLimit);
        void ColorScreen(double netWeight);
        void RemoveScreenColor();
    }
}