﻿namespace Mt.Proxaml.Application.Services.SystemUpdate
{
    public interface ISystemUpdateService
    {
        bool ExecuteUpdate();
    }
}