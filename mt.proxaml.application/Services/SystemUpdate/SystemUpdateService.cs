﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Mt.Proxaml.Application.Services.SystemUpdate
{
    public class SystemUpdateService : ISystemUpdateService
    {
        private const string DestinationPath = @"C:\Service";
        private const string AppPackageExtension = "mtapp";
        private List<string> _availableUpdateFiles = new List<string>();
        private string _sourceDirectory = @"systemupdate";

        public bool ExecuteUpdate()
        {
            bool result = false;
            if (!SystemUpdateFileExists())
            {
                return false;
            }

            foreach (string updateFile in _availableUpdateFiles)
            {
                result = CopyFile(updateFile);
                if (result == false)
                {
                    break;
                }
            }

            return result;
        }

        private static bool CreateDirectory(string path)
        {
            bool result;
            try
            {
                Directory.CreateDirectory(path);
                result = true;
            }
            catch (IOException)
            {
                result = false;
            }

            return result;
        }

        private bool SystemUpdateFileExists()
        {
            if (!CheckDirectoriesExists())
            {
                return false;
            }

            _availableUpdateFiles = Directory.EnumerateFiles(_sourceDirectory).Select(Path.GetFileName)
                .Where(x => x.Contains(AppPackageExtension)).ToList();
            bool hasFiles = _availableUpdateFiles.Count > 0;
            return hasFiles;
        }

        private bool CopyFile(string filename)
        {
            string sourceFile = Path.Combine(_sourceDirectory, filename);
            string destinationFile = Path.Combine(DestinationPath, filename);
            bool result = false;
            try
            {
                File.Copy(sourceFile, destinationFile, true);
                result = true;
            }
            catch (IOException)
            {
            }

            return result;
        }

        private bool CheckDirectoriesExists()
        {
            bool result = true;

            _sourceDirectory =
                Path.Combine(
                    DriveInfo.GetDrives().Where(d => d.DriveType == DriveType.Removable)?.FirstOrDefault()
                        ?.ToString() ?? string.Empty, _sourceDirectory);

            if (Directory.Exists(DestinationPath) == false)
            {
                result = CreateDirectory(DestinationPath);
            }

            if (Directory.Exists(_sourceDirectory) == false)
            {
                result = false;
            }

            return result;
        }
    }
}