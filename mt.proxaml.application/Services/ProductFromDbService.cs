﻿using Mt.Proxaml.Application.Models;

namespace Mt.Proxaml.Application.Services
{
    public static class ProductFromDbService
    {
        public static Product GetProductFromWeightEntry(WeightEntry stockWeightEntry)
        {
            if (stockWeightEntry.KeydataGrossweightValue == null)
            {
                return null;
            }

            Product product = new Product
            {
                Unit = UnitConverterDb.ConvertStringToUnit(stockWeightEntry.KeydataGrossweightUnitId),
                TargetWeight = (double)stockWeightEntry.KeydataGrossweightValue
            };
            return product;
        }
    }
}