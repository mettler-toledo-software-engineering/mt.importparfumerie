﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Timers;
using Mt.Proxaml.Application.Infrastructure;

namespace Mt.Proxaml.Application.Services
{
    public class MeasurementService : IMeasurementService
    {
        private readonly Stopwatch _stopWatch = new Stopwatch();
        private readonly Timer _updateMessageTimer = new Timer();
        private readonly Timer _waitingForStabilizationTimer = new Timer();
        private double _stableWeightDuration;
        private double _minimumWeight;
        private double _countDown;
        private bool _measurementDone = false;
        private double _netWeight;

        public MeasurementService()
        {
            _waitingForStabilizationTimer.AutoReset = false;
            _waitingForStabilizationTimer.Elapsed += OnStableWeight;
            _updateMessageTimer.AutoReset = true;
            _updateMessageTimer.Interval = 100;
            _updateMessageTimer.Elapsed += UpdateMessage;
        }

        public event Action BelowMinimumWeight;
        public event Action<double> UpdateCountDownMessage;
        public event Action<double> MeasurementDone;

        public void UpdateParameters(double stableWeightDuration, double minimumWeight)
        {
            _stableWeightDuration = stableWeightDuration;
            // setting the timer's enabled property to true is necessary to avoid the Elapsed event to be raised after setting the interval
            _waitingForStabilizationTimer.Enabled = true;
            _waitingForStabilizationTimer.Interval = _stableWeightDuration;
            _waitingForStabilizationTimer.Enabled = false;
            _minimumWeight = minimumWeight;
        }

        public void ScaleStoreOnWeightChanged(bool weightIsValid, string netWeightString)
        {
            ResetAllTimers();
            if (!weightIsValid)
            {
                return;
            }

            bool parsedSuccess = double.TryParse(netWeightString, NumberStyles.Number, Globals.Culture,
                out double result);
            if (parsedSuccess && result > _minimumWeight && !_measurementDone)
            {
                StartAllTimers();
                _netWeight = result;
            }

            if (!parsedSuccess || !(result <= _minimumWeight) || !_measurementDone)
            {
                return;
            }

            BelowMinimumWeight?.Invoke();
            _measurementDone = false;
        }

        public void OnStableWeight(object sender, ElapsedEventArgs e)
        {
            ResetAllTimers();
            MeasurementDone?.Invoke(_netWeight);
            _measurementDone = true;
        }

        private void StartAllTimers()
        {
            _waitingForStabilizationTimer?.Start();
            _stopWatch?.Start();
            _updateMessageTimer?.Start();
        }

        private void ResetAllTimers()
        {
            _waitingForStabilizationTimer?.Stop();
            _updateMessageTimer?.Stop();
            _stopWatch?.Reset();
        }

        private void UpdateMessage(object sender, ElapsedEventArgs e)
        {
            _countDown = Math.Round((_stableWeightDuration - _stopWatch.ElapsedMilliseconds) / 1000.0, 1);
            UpdateCountDownMessage?.Invoke(_countDown);
        }
    }
}