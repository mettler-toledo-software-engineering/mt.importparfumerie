﻿namespace Mt.Proxaml.Application.Services
{
    public interface IBarcodeScannerService
    {
        bool BarcodeSanityCheck(string barcode);
        string GetRawBarcode(string barcode);
    }
}