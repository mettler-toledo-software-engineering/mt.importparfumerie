﻿using System.Threading.Tasks;
using Mt.Proxaml.Application.Models;

namespace Mt.Proxaml.Application.Services.DataAccess
{
    public interface IStockobjectbundleService 
    {
        Task<WeightEntry> GetValuesForBarcode(string barcode);
    }
}