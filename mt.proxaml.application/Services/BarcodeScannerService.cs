﻿namespace Mt.Proxaml.Application.Services
{
    public class BarcodeScannerService : IBarcodeScannerService
    {
        public bool BarcodeSanityCheck(string barcode)
        {
            return barcode.Length >= 6 && barcode.Substring(0, 5) == "]C100";
        }

        public string GetRawBarcode(string barcode)
        {
            string rawBarcodeNoFncOrAi = barcode.Remove(0, 5); // remove ]C100 -> count = 5
            return rawBarcodeNoFncOrAi;
        }
    }
}