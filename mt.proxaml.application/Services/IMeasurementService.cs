﻿using System;
using System.Timers;

namespace Mt.Proxaml.Application.Services
{
    public interface IMeasurementService
    {
        event Action BelowMinimumWeight;
        event Action<double> MeasurementDone;
        event Action<double> UpdateCountDownMessage;
        void UpdateParameters(double stableWeightDuration, double minimumWeight);
        void ScaleStoreOnWeightChanged(bool weightIsValid, string netWeightString);
        void OnStableWeight(object sender, ElapsedEventArgs e);
    }
}