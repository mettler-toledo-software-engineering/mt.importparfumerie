﻿namespace mt.proxaml.wpf
{
    public enum Unit
    {
        Gram = 0,
        Kilogram = 1,
        MetricTon = 2,
        Milligram = 3,
        Pound = 4,
        Ounce = 5,
        PoundOunces = 6,
        AvdpTon = 7,
        Custom = 8,
        Pieces = 9,
        Unknown = 10
    }
}