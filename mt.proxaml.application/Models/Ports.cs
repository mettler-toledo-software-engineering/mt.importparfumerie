﻿namespace Mt.Proxaml.Application.Models
{
    public enum Ports
    {
        Port2,
        Port3,
        Port4,
        Port5,
        Port6
    }

    public static class PortConverter
    {
        public static int ConvertPortsToInt(this Ports port)
        {
            switch (port)
            {
                case Ports.Port2:
                    return 2;
                case Ports.Port3:
                    return 3;
                case Ports.Port4:
                    return 4;
                case Ports.Port5:
                    return 5;
                case Ports.Port6:
                    return 6;
                default:
                    return 5;
            }
        }
    }
}