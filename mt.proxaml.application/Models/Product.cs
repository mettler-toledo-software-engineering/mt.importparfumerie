﻿namespace Mt.Proxaml.Application.Models
{
    /// <summary>
    ///     example model for a transaction, implements the domain object in order to receive an Id for DB access
    /// </summary>
    public class Product
    {
        public double TargetWeight { get; set; }

        public Unit Unit { get; set; }
    }
}