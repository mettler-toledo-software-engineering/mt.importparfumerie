﻿namespace Mt.Proxaml.Application.Models
{
    public enum Unit
    {
        Gram = 0,
        Kilogram = 1
    }

    public static class UnitConverterDb
    {
        public static string ConvertUnitToString(this Unit unit)
        {
            switch (unit)
            {
                case Unit.Gram:
                    return "g";
                case Unit.Kilogram:
                    return "kg";
                default:
                    return string.Empty;
            }
        }

        public static Unit ConvertStringToUnit(string unit)
        {
            switch (unit)
            {
                case "g":
                    return Unit.Gram;
                case "kg":
                    return Unit.Kilogram;
                default:
                    return Unit.Kilogram;
            }
        }
    }
}