﻿// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Mt.Proxaml.Application.Models
{
    public class WeightEntry
    {
        public string Luid { get; set; }
        public decimal? KeydataGrossweightValue { get; set; }
        public string KeydataGrossweightUnitId { get; set; }
    }
}