﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ModalControl
{
    /// <summary>
    ///     Follow steps 1a or 1b and then 2 to use this custom control in a XAML file.
    ///     Step 1a) Using this custom control in a XAML file that exists in the current project.
    ///     Add this XmlNamespace attribute to the root element of the markup file where it is
    ///     to be used:
    ///     xmlns:MyNamespace="clr-namespace:ModalControl"
    ///     Step 1b) Using this custom control in a XAML file that exists in a different project.
    ///     Add this XmlNamespace attribute to the root element of the markup file where it is
    ///     to be used:
    ///     xmlns:MyNamespace="clr-namespace:ModalControl;assembly=ModalControl"
    ///     You will also need to add a project reference from the project where the XAML file lives
    ///     to this project and Rebuild to avoid compilation errors:
    ///     Right click on the target project in the Solution Explorer and
    ///     "Add Reference"->"Projects"->[Select this project]
    ///     Step 2)
    ///     Go ahead and use your control in the XAML file.
    ///     <MyNamespace:CustomControl1 />
    /// </summary>
    public class Modal : ContentControl
    {
        // Using a DependencyProperty as the backing store for ModelIsOpen.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ModelIsOpenProperty =
            DependencyProperty.Register("ModelIsOpen", typeof(bool), typeof(Modal), new PropertyMetadata(false));


        static Modal()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Modal), new FrameworkPropertyMetadata(typeof(Modal)));
            BackgroundProperty.OverrideMetadata(
                typeof(Modal),
                new FrameworkPropertyMetadata(CreateDefaultBackground()));
        }


        public bool ModelIsOpen
        {
            get => (bool)GetValue(ModelIsOpenProperty);
            set => SetValue(ModelIsOpenProperty, value);
        }

        private static object CreateDefaultBackground()
        {
            return new SolidColorBrush(Colors.Black)
            {
                Opacity = 0.3
            };
        }
    }
}