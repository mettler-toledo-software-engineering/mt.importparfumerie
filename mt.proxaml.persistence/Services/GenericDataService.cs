﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using mt.proxaml.application.Models;
using Mt.Proxaml.Application.Services.DataAccess;
using Mt.Proxaml.persistence.Context;
using Mt.Proxaml.persistence.Services.Common;

namespace Mt.Proxaml.persistence.Services
{
    public class GenericDataService<T> : IDataService<T> where T : DomainObject
    {
        protected readonly ModelContextFactory ContextFactory;
        private readonly NonQueryDataService<T> _nonQueryDataService;

        protected GenericDataService(ModelContextFactory contextFactory)
        {
            ContextFactory = contextFactory;
            _nonQueryDataService = new NonQueryDataService<T>(contextFactory);
        }

        public async Task<T> Create(T entity)
        {
            return await _nonQueryDataService.Create(entity);
        }

        public async Task<bool> Delete(int id)
        {
            return await _nonQueryDataService.Delete(id);
        }

        public async Task<T> Update(int id, T entity)
        {
            return await _nonQueryDataService.Update(id, entity);
        }

        public async Task<T> Get(int id)
        {
            using (ModelContext context = ContextFactory.CreateDbContext())
            {
                T entity = await context.Set<T>().FirstOrDefaultAsync(e => e.Id == id);
                return entity;
            }
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            using (ModelContext context = ContextFactory.CreateDbContext())
            {
                IEnumerable<T> entities = await context.Set<T>().ToListAsync();
                return entities;
            }
        }
    }
}