﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using mt.proxaml.application.Models;
using Mt.Proxaml.persistence.Context;

namespace Mt.Proxaml.persistence.Services.Common
{
    public class NonQueryDataService<T> where T : DomainObject
    {
        private readonly ModelContextFactory _contextFactory;

        public NonQueryDataService(ModelContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public async Task<T> Create(T entity)
        {
            using (ModelContext context = _contextFactory.CreateDbContext())
            {
                var createdResult = await context.Set<T>().AddAsync(entity);
                await context.SaveChangesAsync();
                return createdResult.Entity;
            }
        }

        public async Task<T> Update(int id, T entity)
        {
            using (ModelContext context = _contextFactory.CreateDbContext())
            {
                entity.Id = id;

                context.Set<T>().Update(entity);
                await context.SaveChangesAsync();

                return entity;
            }
        }

        public async Task<bool> Delete(int id)
        {
            using (ModelContext context = _contextFactory.CreateDbContext())
            {
                T entity = await context.Set<T>().FirstOrDefaultAsync(e => e.Id == id);
                context.Set<T>().Remove(entity);
                await context.SaveChangesAsync();

                return true;
            }
        }
    }
}