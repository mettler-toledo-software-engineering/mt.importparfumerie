﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Mt.Proxaml.Application.Models;
using Mt.Proxaml.Application.Services.DataAccess;
using Mt.Proxaml.persistence.Context;

namespace Mt.Proxaml.persistence.Services
{
    /// <summary>
    ///     specific dataservice for transaction that are not covered by the generic dataservice
    /// </summary>
    public class StockobjectbundleDataService : IStockobjectbundleService
    {
        private readonly ModelContextFactory _contextFactory;

        public StockobjectbundleDataService(ModelContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public async Task<WeightEntry> GetValuesForBarcode(string barcode)
        {
            using (ModelContext context = _contextFactory.CreateDbContext())
            {
                WeightEntry transaction = await context.StockObjectBundle
                    .Where(e => e.Luid.Equals(barcode))
                    .FirstOrDefaultAsync();

                return transaction;
            }
        }
    }
}