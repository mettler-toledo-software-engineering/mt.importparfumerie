﻿using System;
using Microsoft.EntityFrameworkCore;

namespace Mt.Proxaml.persistence.Context
{
    public class ModelContextFactory
    {
        private readonly DbContextOptionsBuilder<ModelContext> _options;

        public ModelContextFactory()
        {
            _options = new DbContextOptionsBuilder<ModelContext>();
        }

        public Action<DbContextOptionsBuilder> ConfigureDbContext { get; set; }

        public ModelContext CreateDbContext()
        {
            ConfigureDbContext(_options);
            return new ModelContext(_options.Options);
        }
    }
}