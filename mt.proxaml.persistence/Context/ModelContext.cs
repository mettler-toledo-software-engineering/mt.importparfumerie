﻿using Microsoft.EntityFrameworkCore;
using Mt.Proxaml.Application.Models;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Mt.Proxaml.persistence
{
    public class ModelContext : DbContext
    {
        public ModelContext()
        {
        }

        public ModelContext(DbContextOptions<ModelContext> options)
            : base(options)
        {
        }

        public DbSet<WeightEntry> StockObjectBundle { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:DefaultSchema", "W5_PROD");

            modelBuilder.Entity<WeightEntry>(entity =>
            {
                entity.HasKey(e => new { e.Luid });

                entity.ToTable("STOCKOBJECTBUNDLE");

                entity.Property(e => e.Luid)
                    .HasColumnName("LUID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.KeydataGrossweightUnitId)
                    .HasColumnName("KEYDATA_GROSSWEIGHT_UNIT_ID")
                    .HasMaxLength(5);

                entity.Property(e => e.KeydataGrossweightValue)
                    .HasColumnName("KEYDATA_GROSSWEIGHT_VALUE")
                    .HasColumnType("NUMBER(12,3)")
                    .HasDefaultValueSql("0");
            });

            base.OnModelCreating(modelBuilder);
        }
    }
}