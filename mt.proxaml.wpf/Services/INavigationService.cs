﻿namespace Mt.Proxaml.Wpf.Services
{
    public interface INavigationService
    {
        void Navigate();
    }
}