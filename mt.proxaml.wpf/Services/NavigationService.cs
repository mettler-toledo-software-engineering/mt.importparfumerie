﻿using System;
using Mt.Proxaml.Wpf.Stores;
using Mt.Proxaml.Wpf.ViewModels;

namespace Mt.Proxaml.Wpf.Services
{
    public class NavigationService<TViewModel> : INavigationService where TViewModel : SimpleViewModelBase
    {
        private readonly Func<TViewModel> _createViewModel;
        private readonly NavigationStore _navigationStore;

        public NavigationService(NavigationStore navigationStore, Func<TViewModel> createViewModel)
        {
            _navigationStore = navigationStore;
            _createViewModel = createViewModel;
        }

        public void Navigate()
        {
            _navigationStore.CurrentViewModel = _createViewModel();
        }
    }
}