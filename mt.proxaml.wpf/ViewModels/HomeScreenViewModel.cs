﻿using System.Diagnostics;
using System.Threading.Tasks;
using Mt.Proxaml.Wpf.CompositionContainerExtensions;
using Mt.Proxaml.Wpf.Services;
using Mt.Proxaml.Wpf.Stores;
using MT.Singularity.Composition;
using MT.Singularity.Expressions;
using MT.Singularity.Platform.UI;

namespace Mt.Proxaml.Wpf.ViewModels
{
    public class HomeScreenViewModel : ViewModelBase
    {
        private readonly BindingsContainer _bindings = new BindingsContainer();

        private readonly CompositionContainer _compositionContainer;

        private readonly ModalNavigationStore _modalNavigationStore;
        private readonly NavigationStore _navigationStore;

        private ShutdownStore _shutdownStore;

        public HomeScreenViewModel(IObjectResolver compositionContainer) : base(compositionContainer)
        {
            _compositionContainer = compositionContainer.Resolve<CompositionContainer>();
            _navigationStore = compositionContainer.Resolve<NavigationStore>();
            _modalNavigationStore = compositionContainer.Resolve<ModalNavigationStore>();
            _navigationStore.CurrentViewModelChanged += OnCurrentViewModelChanged;
            _modalNavigationStore.CurrentViewModelChanged += OnCurrentModalViewModelChanged;
        }

        public SimpleViewModelBase CurrentViewModel => _navigationStore.CurrentViewModel;

        public SimpleViewModelBase CurrentModalViewModel => _modalNavigationStore.CurrentViewModel;

        public bool ModalIsOpen => _modalNavigationStore.ModalIsOpen;

        public GlobalMessageViewModel GlobalMessageViewModel => _compositionContainer.Resolve<GlobalMessageViewModel>();

        public DatabankStatusViewModel DatabankStatusViewModel =>
            _compositionContainer.Resolve<DatabankStatusViewModel>();

        public override Task InitializeAsync(object context)
        {
            _compositionContainer.EnableRecursiveResolution();
            _compositionContainer
                .AddServicesToCompositionContainer()
                .AddStoresToCompositionContainer()
                .AddViewModelsToCompositionContainer()
                .AddViewsToCompositionContainer();

            _shutdownStore = _compositionContainer.Resolve<ShutdownStore>();
            _shutdownStore.ShutDownRequested += HomeScreenViewModel_ShutDownRequested;

            // navigating to the first view with actual content
            INavigationService firstNavigation = _compositionContainer.Resolve<INavigationService>();

            firstNavigation.Navigate();

            return Task.CompletedTask;
        }

        protected override async Task OnShutdownAsync()
        {
            _bindings.Stop();
            await base.OnShutdownAsync();
            // close application for debugging, on terminal restart 
            if (Debugger.IsAttached)
            {
                Process.GetCurrentProcess().Kill();
            }
            else
            {
                StartShutDown();
            }
        }

        private void StartShutDown()
        {
            ProcessStartInfo proc = new ProcessStartInfo
            {
                FileName = "cmd",
                WindowStyle = ProcessWindowStyle.Hidden,
                Arguments = "/C shutdown -f -r -t 5"
            };
            Process.Start(proc);
        }

        private async void HomeScreenViewModel_ShutDownRequested()
        {
            await OnShutdownAsync();
        }

        private void OnCurrentViewModelChanged()
        {
            NotifyPropertyChanged(nameof(CurrentViewModel));
        }

        private void OnCurrentModalViewModelChanged()
        {
            NotifyPropertyChanged(nameof(CurrentModalViewModel));
            NotifyPropertyChanged(nameof(ModalIsOpen));
        }
    }
}