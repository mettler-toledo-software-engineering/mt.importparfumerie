﻿using System;
using MT.Singularity.Data;

namespace Mt.Proxaml.Wpf.ViewModels
{
    /// <summary>
    ///     this base can be used, when no singularity features are necessary
    /// </summary>
    public abstract class SimpleViewModelBase : PropertyChangedBase, IDisposable
    {
        public virtual void Dispose()
        {
        }
    }
}