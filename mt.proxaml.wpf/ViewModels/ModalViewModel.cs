﻿using System.Reflection;
using System.Windows.Input;
using Mt.Proxaml.Wpf.Commands;
using Mt.Proxaml.Wpf.Services;

namespace Mt.Proxaml.Wpf.ViewModels
{
    /// <summary>
    ///     simple example viewmodel on how to display and close a modal
    ///     this can be used if more individualization is necessary that the userinteraction can provide
    /// </summary>
    public class ModalViewModel : SimpleViewModelBase
    {
        public ModalViewModel(INavigationService closeModalNavigationService)
        {
            CloseModalCommand = new CloseModalCommand(closeModalNavigationService);
        }

        public string Version => $"Version: {Assembly.GetExecutingAssembly().GetName().Version}";

        private ICommand CloseModalCommand { get; }
    }
}