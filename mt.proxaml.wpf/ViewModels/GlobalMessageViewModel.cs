﻿using System.Timers;
using Mt.Proxaml.Wpf.Stores;

namespace Mt.Proxaml.Wpf.ViewModels
{
    public class GlobalMessageViewModel : SimpleViewModelBase
    {
        private readonly GlobalMessageStore _globalMessageStore;
        private Timer _timer;

        public GlobalMessageViewModel(GlobalMessageStore globalMessageStore)
        {
            _globalMessageStore = globalMessageStore;
            _globalMessageStore.CurrentMessageChanged += OnCurrentMessageChanged;
            StartTimer();
        }

        public string Message => _globalMessageStore.CurrentMessage;
        public bool HasMessage => !string.IsNullOrWhiteSpace(_globalMessageStore.CurrentMessage);
        public bool IsErrorMessage => _globalMessageStore.CurrentMessageType == GlobalMessageStore.MessageType.Error;
        public bool IsInfoMessage => _globalMessageStore.CurrentMessageType == GlobalMessageStore.MessageType.Info;

        public bool IsSuccessMessage =>
            _globalMessageStore.CurrentMessageType == GlobalMessageStore.MessageType.Success;

        public bool IsNoneMessage => _globalMessageStore.CurrentMessageType == GlobalMessageStore.MessageType.None;

        public override void Dispose()
        {
            _globalMessageStore.CurrentMessageChanged -= OnCurrentMessageChanged;
            if (_timer != null)
            {
                _timer.Elapsed -= OnTimerElapsed;
            }
        }

        private void StartTimer()
        {
            _timer = new Timer
            {
                Interval = _globalMessageStore.MessageDuration,
                Enabled = true,
                AutoReset = false
            };
            _timer.Elapsed += OnTimerElapsed;
        }

        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            _globalMessageStore.UpdateMessage(string.Empty, GlobalMessageStore.MessageType.None);
        }

        private void OnCurrentMessageChanged()
        {
            if (IsNoneMessage == false)
            {
                _timer.Interval = _globalMessageStore.MessageDuration;
                _timer?.Start();
            }

            NotifyPropertyChanged(nameof(Message));
            NotifyPropertyChanged(nameof(HasMessage));
            NotifyPropertyChanged(nameof(IsErrorMessage));
            NotifyPropertyChanged(nameof(IsInfoMessage));
            NotifyPropertyChanged(nameof(IsSuccessMessage));
            NotifyPropertyChanged(nameof(IsNoneMessage));
        }
    }
}