﻿using System.Windows.Media;
using Mt.Proxaml.Application.Models;
using Mt.Proxaml.Wpf.Commands;
using Mt.Proxaml.Wpf.Stores;
using MT.Singularity.Platform.Ribbon;
using MT.Singularity.Platform.UI.Shell.Ribbon;

namespace Mt.Proxaml.Wpf.ViewModels
{
    public class FirstViewModel : RibbonViewModelBase
    {
        private readonly ApplicationSetupStore _applicationSetupStore;
        private readonly UserStore _userStore;
        private RibbonItem _shutDownKey;

        public FirstViewModel(
            RibbonItemStore ribbonItemStore,
            ProcessFlowStore processFlowStore,
            ProductStore productStore, BarcodeScannerStore barcodeScannerStore, ShutdownStore shutdownStore,
            UserStore userStore, GlobalMessageStore globalMessageStore, ApplicationSetupStore applicationSetupStore) :
            base(ribbonItemStore)
        {
            processFlowStore.ColorChanged += ProcessFlowServiceOnProcessFlowChanged;
            processFlowStore.ClearDisplayedValues += ProcessFlowServiceClearDisplayedValues;
            _applicationSetupStore = applicationSetupStore;
            _applicationSetupStore.ToleranceChanged += ApplicationProcessServiceToleranceChanged;
            productStore.ProductReceived += ProductStoreOnTargetWeightReceived;
            _userStore = userStore;
            _userStore.AdminRightsGiven += UserStore_AdminRightsGiven;
            _userStore.AdminRightsRemoved += UserStore_AdminRightsRemoved;

            ShutDownMethod = new ShutDownAsyncCommand(barcodeScannerStore, shutdownStore, globalMessageStore);

            AddRibbonItemsDefault();
        }

        public string TargetValueDisplay => TargetValue != 0 ? $"{TargetValue} {Unit}" : string.Empty;

        public string ToleranceDisplay => UpperLimit != 0 && LowerLimit != 0
            ? $"{LowerLimit} {Unit} - {UpperLimit} {Unit}"
            : string.Empty;

        private IRibbonCommand ShutDownMethod { get; }

        private Brush _color;

        public Brush Color
        {
            get => _color;
            set => SetField(ref _color, value);
        }

        private double _targetValue;

        public double TargetValue
        {
            get => _targetValue;
            set => SetField(ref _targetValue, value);
        }

        private double _upperLimit;

        public double UpperLimit
        {
            get => _upperLimit;
            set => SetField(ref _upperLimit, value);
        }

        private double _lowerLimit;

        public double LowerLimit
        {
            get => _lowerLimit;
            set => SetField(ref _lowerLimit, value);
        }

        private string _unit;

        public string Unit
        {
            get => _unit;
            set => SetField(ref _unit, value);
        }

        protected override void AddRibbonItemsDefault()
        {
            base.AddRibbonItemsDefault();

            _shutDownKey = new RibbonItem("shutdown")
            {
                ImageSource = "../Images/ShutDown.png",
                Command = ShutDownMethod,
                IsEnabled = _userStore.IsAdmin
            };

            _ribbonItems.ReplaceRibbonItem(7, _shutDownKey);
        }

        private void UserStore_AdminRightsRemoved()
        {
            _shutDownKey.IsEnabled = false;
        }

        private void UserStore_AdminRightsGiven()
        {
            _shutDownKey.IsEnabled = true;
        }

        private void ProcessFlowServiceClearDisplayedValues()
        {
            TargetValue = 0;
            UpperLimit = 0;
            LowerLimit = 0;
            NotifyPropertyChanged(nameof(ToleranceDisplay));
            NotifyPropertyChanged(nameof(TargetValueDisplay));
        }

        private void ProductStoreOnTargetWeightReceived(Product product)
        {
            TargetValue = product.TargetWeight;
            SetLimits(product.TargetWeight, _applicationSetupStore.CurrentCustomSetupConfiguration.TolerancePercentage);
            Unit = product.Unit.ConvertUnitToString();
            NotifyPropertyChanged(nameof(ToleranceDisplay));
            NotifyPropertyChanged(nameof(TargetValueDisplay));
        }

        private void ProcessFlowServiceOnProcessFlowChanged(Brush brush)
        {
            Color = brush;
        }

        private void ApplicationProcessServiceToleranceChanged(double tolerance)
        {
            SetLimits(TargetValue, tolerance);
            NotifyPropertyChanged(nameof(ToleranceDisplay));
        }

        private void SetLimits(double target, double tolerancePercentage)
        {
            double tolerance = (target * tolerancePercentage) / 100;
            UpperLimit = TargetValue + tolerance;
            LowerLimit = TargetValue - tolerance;
        }
    }
}