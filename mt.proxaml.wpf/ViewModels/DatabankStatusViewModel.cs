﻿using System.Windows.Input;
using Mt.Proxaml.Application.Services;
using Mt.Proxaml.Wpf.Commands;
using Mt.Proxaml.Wpf.Stores;

namespace Mt.Proxaml.Wpf.ViewModels
{
    public class DatabankStatusViewModel : SimpleViewModelBase
    {
        private readonly ConnectionStringStore _connectionStringStore;
        private readonly GlobalMessageStore _globalMessageStore;

        public DatabankStatusViewModel(
            ConnectionStringStore connectionStringStore,
            GlobalMessageStore globalMessageStore)
        {
            _globalMessageStore = globalMessageStore;
            _connectionStringStore = connectionStringStore;
            _connectionStringStore.DatabaseStatusChanged += DatabaseStatusChanged;
            _connectionStringStore.ConnectionStringChanged += SetDatabaseDisplayInfo;

            GetInitialDatabaseStatus();
        }

        public ICommand TestConnection { get; set; }

        private string _databankStatus;

        public string DatabankStatus
        {
            get => _databankStatus;
            set => SetField(ref _databankStatus, value);
        }

        private string _databaseDisplayInfo;

        public string DatabaseDisplayInfo
        {
            get => _databaseDisplayInfo;
            set => SetField(ref _databaseDisplayInfo, value);
        }

        private void GetInitialDatabaseStatus()
        {
            string connectionString = SerializationService.GetConnectionString();
            _connectionStringStore.TestConnectionString(false, connectionString);

            TestConnection = new TestConnectionCommand(_connectionStringStore, _globalMessageStore);
            NotifyPropertyChanged(nameof(TestConnection));
            SetDatabaseDisplayInfo();
        }

        private void SetDatabaseDisplayInfo()
        {
            DatabaseDisplayInfo = _connectionStringStore.DatabaseInfoDisplay;
        }

        private void DatabaseStatusChanged(string status)
        {
            DatabankStatus = status;
        }
    }
}