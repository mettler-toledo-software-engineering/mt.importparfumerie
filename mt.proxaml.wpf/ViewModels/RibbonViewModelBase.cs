﻿using System.Collections.ObjectModel;
using Mt.Proxaml.Wpf.CustomItems;
using Mt.Proxaml.Wpf.Stores;
using MT.Singularity.Platform.UI.Component.Ribbon;

namespace Mt.Proxaml.Wpf.ViewModels
{
    public class RibbonViewModelBase : SimpleViewModelBase
    {
        protected readonly ExtendedObservableCollection<IRibbonItem> _ribbonItems =
            new ExtendedObservableCollection<IRibbonItem>();

        protected readonly RibbonItemStore RibbonItemStore;

        public RibbonViewModelBase(RibbonItemStore ribbonItemStore)
        {
            RibbonItemStore = ribbonItemStore;
        }

        public ObservableCollection<IRibbonItem> RibbonItems => _ribbonItems;

        protected virtual void AddRibbonItemsDefault()
        {
            _ribbonItems.Clear();
            _ribbonItems.Add(RibbonItemStore.ZeroKey);
            _ribbonItems.Add(RibbonItemStore.TareKey);
            _ribbonItems.Add(RibbonItemStore.ClearKey);
            _ribbonItems.Add(RibbonItemStore.PlaceHolderKey);
            _ribbonItems.Add(RibbonItemStore.PlaceHolderKey);
            _ribbonItems.Add(RibbonItemStore.PlaceHolderKey);
            _ribbonItems.Add(RibbonItemStore.PlaceHolderKey);
            _ribbonItems.Add(RibbonItemStore.PlaceHolderKey);
        }

        protected virtual void AddRibbonItemsEmpty()
        {
            _ribbonItems.Clear();
            _ribbonItems.Add(RibbonItemStore.PlaceHolderKey);
            _ribbonItems.Add(RibbonItemStore.PlaceHolderKey);
            _ribbonItems.Add(RibbonItemStore.PlaceHolderKey);
            _ribbonItems.Add(RibbonItemStore.PlaceHolderKey);
            _ribbonItems.Add(RibbonItemStore.PlaceHolderKey);
            _ribbonItems.Add(RibbonItemStore.PlaceHolderKey);
            _ribbonItems.Add(RibbonItemStore.PlaceHolderKey);
            _ribbonItems.Add(RibbonItemStore.PlaceHolderKey);
        }
    }
}