﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using Microsoft.VisualStudio.Threading;
using MT.Singularity.Platform.UI.Shell.Ribbon;

namespace Mt.Proxaml.Wpf.CustomItems
{
    public class ExtendedObservableCollection<T> : ObservableCollection<T>
    {
        private readonly JoinableTaskFactory _joinableTaskFactory =
            new JoinableTaskFactory(new JoinableTaskContext());

        public bool EnableRibbonItem(int index, bool enable)
        {
            if (index >= Count)
            {
                return false;
            }

            var ribbonItem = this[index] as RibbonItem;
            ribbonItem.IsEnabled = enable;
            return true;
        }

        public bool ReplaceRibbonItem(int index, T iRibbonItem)
        {
            var success = false;
            _joinableTaskFactory.Run(async () =>
            {
                await _joinableTaskFactory.SwitchToMainThreadAsync();
                if (index >= Count)
                {
                    return;
                }

                RemoveAt(index);
                Insert(index, iRibbonItem);
                success = true;
            });
            return success;
        }

        public void Overwrite(IEnumerable<T> range)
        {
            Items.Clear();
            AddRange(range);
        }

        private void AddRange(IEnumerable<T> range)
        {
            foreach (var item in range)
            {
                Items.Add(item);
            }

            OnPropertyChanged(new PropertyChangedEventArgs(nameof(Count)));
            OnPropertyChanged(new PropertyChangedEventArgs("Item[]"));
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }
    }
}