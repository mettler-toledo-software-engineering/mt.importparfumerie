﻿using System.Windows.Controls;

namespace Mt.Proxaml.Wpf.Views
{
    /// <summary>
    ///     Interaction logic for ModalView.xaml
    /// </summary>
    public partial class ModalView : UserControl
    {
        public ModalView()
        {
            InitializeComponent();
        }
    }
}