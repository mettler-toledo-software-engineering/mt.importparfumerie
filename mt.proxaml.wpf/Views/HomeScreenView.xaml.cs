﻿using System;
using MT.Singularity.Platform.UI.Navigation;

namespace Mt.Proxaml.Wpf.Views
{
    /// <summary>
    ///     None of the methods are needed and are used in any way.
    /// </summary>
    public partial class HomeScreenView : INavigationPage
    {
        public HomeScreenView()
        {
            InitializeComponent();
        }

        /// <inheritdoc />
        public event EventHandler CloseRequested;

        /// <inheritdoc />
        public void OnNavigatedTo(NavigationAction action)
        {
        }

        /// <inheritdoc />
        public NavigationResult OnNavigatingFrom(NavigationAction action)
        {
            return NavigationResult.Proceed;
        }

        /// <inheritdoc />
        public void OnNavigatedFrom(NavigationAction action)
        {
        }

        protected virtual void OnCloseRequested()
        {
            CloseRequested?.Invoke(this, EventArgs.Empty);
        }
    }
}