﻿using System.Windows.Controls;

namespace Mt.Proxaml.Wpf.Views
{
    /// <summary>
    ///     Interaction logic for FirstView.xaml
    /// </summary>
    public partial class FirstView : UserControl
    {
        public FirstView()
        {
            InitializeComponent();
        }
    }
}