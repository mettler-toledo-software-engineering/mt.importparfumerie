﻿using Mt.Proxaml.Wpf.Services;

namespace Mt.Proxaml.Wpf.Commands
{
    public class CloseModalCommand : CommandBase
    {
        private readonly INavigationService _closeModalNavigationService;

        public CloseModalCommand(INavigationService closeModalNavigationService)
        {
            _closeModalNavigationService = closeModalNavigationService;
        }

        public override void Execute(object parameter)
        {
            _closeModalNavigationService.Navigate();
        }
    }
}