﻿using System;
using Mt.Proxaml.Application.Services.SystemUpdate;
using Mt.Proxaml.Wpf.SetupNodes.SubNode1;
using Mt.Proxaml.Wpf.Stores;

namespace Mt.Proxaml.Wpf.Commands
{
    public class UpdateSystemCommand : CommandBase
    {
        private readonly ISystemUpdateService _systemUpdateService;
        private readonly CustomSetupNodeAViewModel _viewModel;
        private readonly GlobalMessageStore _messageStore;
        private readonly UserStore _userStore;

        public UpdateSystemCommand(
            CustomSetupNodeAViewModel viewModel, ISystemUpdateService systemUpdateService,
            GlobalMessageStore messageStore, UserStore userStore)
        {
            _viewModel = viewModel;
            _systemUpdateService = systemUpdateService;
            _messageStore = messageStore;
            _userStore = userStore;
        }

        public override bool CanExecute(object parameter)
        {
            return _userStore.IsAdmin;
        }

        public override void Execute(object parameter)
        {
            try
            {
                bool success = _systemUpdateService.ExecuteUpdate();
                if (success)
                {
                    _viewModel.UpdateResult = "Update erfolgreich";
                    return;
                }

                _viewModel.UpdateResult = "Update gescheitert";
            }
            catch (Exception e)
            {
                _viewModel.UpdateResult = "Update gescheitert";
                _messageStore.UpdateMessage("Fehler beim Versuch zu Updaten", GlobalMessageStore.MessageType.Error, e);
            }
        }
    }
}