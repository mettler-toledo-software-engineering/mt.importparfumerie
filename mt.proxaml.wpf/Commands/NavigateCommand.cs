﻿using System.Threading.Tasks;
using Mt.Proxaml.Wpf.Services;
using MT.Singularity.Platform.Ribbon;

namespace Mt.Proxaml.Wpf.Commands
{
    public class NavigateCommand : CommandBase, IRibbonCommand
    {
        private readonly bool _canExecute = true;
        private readonly INavigationService _navigationService;

        public NavigateCommand(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        bool IRibbonCommand.CanExecute => CanExecute(null);

        public Task ExecuteCommandAsync()
        {
            Execute(null);
            return Task.CompletedTask;
        }

        public override bool CanExecute(object parameter)
        {
            return base.CanExecute(parameter) && _canExecute;
        }


        public override void Execute(object parameter)
        {
            _navigationService.Navigate();
        }
    }
}