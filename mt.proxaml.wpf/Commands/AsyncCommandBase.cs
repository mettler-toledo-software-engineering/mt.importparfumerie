﻿using System;
using System.Threading.Tasks;
using MT.Singularity.Platform.Ribbon;

namespace Mt.Proxaml.Wpf.Commands
{
    /// <summary>
    ///     multi purpose base command, can be used as Regular ICommand or IRibbonCommand
    ///     Sets Can Execute to false as long as a Task is running async
    /// </summary>
    public abstract class AsyncCommandBase : CommandBase, IRibbonCommand
    {
        private readonly Action<Exception> _onException;

        private bool _isExecuting;

        protected AsyncCommandBase(Action<Exception> onException = null)
        {
            _onException = onException;
        }

        bool IRibbonCommand.CanExecute => CanExecute(null);

        private bool IsExecuting
        {
            get => _isExecuting;
            set
            {
                _isExecuting = value;
                OnCanExecuteChanged();
            }
        }

        async Task IRibbonCommand.ExecuteCommandAsync()
        {
            await ExecutingAsync();
        }

        public override bool CanExecute(object parameter)
        {
            return !IsExecuting && base.CanExecute(parameter);
        }

        public override async void Execute(object parameter)
        {
            await ExecutingAsync();
        }

        protected abstract Task ExecuteAsync(object parameter);

        private async Task ExecutingAsync()
        {
            IsExecuting = true;

            try
            {
                await ExecuteAsync(null);
            }
            catch (Exception ex)
            {
                _onException?.Invoke(ex);
            }
            finally
            {
                IsExecuting = false;
            }
        }
    }
}