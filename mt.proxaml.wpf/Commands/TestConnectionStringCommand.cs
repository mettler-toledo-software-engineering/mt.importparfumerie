﻿using System;
using Mt.Proxaml.Wpf.SetupNodes.SubNode2;
using Mt.Proxaml.Wpf.Stores;

namespace Mt.Proxaml.Wpf.Commands
{
    public class TestConnectionStringCommand : CommandBase
    {
        private readonly ConnectionStringStore _connectionStringStore;
        private readonly DatabaseNodeViewModel _databaseNodeViewModel;
        private readonly GlobalMessageStore _messageStore;
        private readonly UserStore _userStore;

        public TestConnectionStringCommand(
            DatabaseNodeViewModel databaseNodeViewModel, ConnectionStringStore connectionStringStore,
            GlobalMessageStore messageStore, UserStore userStore)
        {
            _connectionStringStore = connectionStringStore;
            _databaseNodeViewModel = databaseNodeViewModel;
            _messageStore = messageStore;
            _userStore = userStore;
        }

        public override bool CanExecute(object parameter)
        {
            return _userStore.IsAdmin;
        }

        public override void Execute(object parameter)
        {
            try
            {
                string connection = _databaseNodeViewModel.ConnectionString;
                _connectionStringStore.TestConnectionStringButton(connection);
            }
            catch (Exception e)
            {
                _messageStore.UpdateMessage(
                    "Fehler beim Testen der Datenbankverbindung",
                    GlobalMessageStore.MessageType.Error, e);
            }
        }
    }
}