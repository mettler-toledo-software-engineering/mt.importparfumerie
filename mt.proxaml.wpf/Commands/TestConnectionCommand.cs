﻿using System;
using Mt.Proxaml.Wpf.Stores;

namespace Mt.Proxaml.Wpf.Commands
{
    public class TestConnectionCommand : CommandBase
    {
        private readonly ConnectionStringStore _connectionStringStore;
        private readonly GlobalMessageStore _messageStore;

        public TestConnectionCommand(ConnectionStringStore connectionStringStore, GlobalMessageStore messageStore)
        {
            _connectionStringStore = connectionStringStore;
            _messageStore = messageStore;
        }

        public override void Execute(object parameter)
        {
            try
            {
                _connectionStringStore.TestCurrentConnectionString(false);
            }
            catch (Exception e)
            {
                _messageStore.UpdateMessage(
                    "Fehler beim Testen der Datenbankverbindung",
                    GlobalMessageStore.MessageType.Error, e);
            }
        }
    }
}