﻿using System;
using System.Threading.Tasks;
using Mt.Proxaml.Wpf.Stores;

namespace Mt.Proxaml.Wpf.Commands
{
    public class ShutDownAsyncCommand : AsyncCommandBase
    {
        private readonly BarcodeScannerStore _barcodeScannerStore;
        private readonly ShutdownStore _shutdownStore;
        private readonly GlobalMessageStore _messageStore;

        public ShutDownAsyncCommand(BarcodeScannerStore barcodeScannerStore, ShutdownStore shutdownStore, GlobalMessageStore messageStore)
        {
            _barcodeScannerStore = barcodeScannerStore;
            _shutdownStore = shutdownStore;
            _messageStore = messageStore;
        }

        protected override async Task ExecuteAsync(object parameter)
        {
            try
            {
                await _barcodeScannerStore.CloseScannerPortAsync();
                _shutdownStore.ShutDown();
            }
            catch (Exception e)
            {
                _messageStore.UpdateMessage("Fehler beim Schliessen des Ports oder beim Herunterfahren", GlobalMessageStore.MessageType.Error, e);
            }
        }
    }
}