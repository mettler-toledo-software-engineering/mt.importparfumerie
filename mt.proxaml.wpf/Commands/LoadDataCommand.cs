﻿using System;
using System.Threading.Tasks;
using Mt.Proxaml.Wpf.Stores;

namespace Mt.Proxaml.Wpf.Commands
{
    public class LoadDataCommand : AsyncCommandBase
    {
        private readonly IDataStore _dataStore;
        private readonly GlobalMessageStore _messageStore;

        public LoadDataCommand(IDataStore dataStoreStore, GlobalMessageStore messageStore)
        {
            _dataStore = dataStoreStore;
            _messageStore = messageStore;
        }

        protected override async Task ExecuteAsync(object parameter)
        {
            try
            {
                await _dataStore.LoadDataAsync();
            }
            catch (Exception e)
            {
                _messageStore.UpdateMessage("Daten konnten nicht geladen werden", GlobalMessageStore.MessageType.Error, e);
            }
        }
    }
}