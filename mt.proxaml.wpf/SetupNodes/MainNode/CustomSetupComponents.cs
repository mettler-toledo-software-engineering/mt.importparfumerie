﻿using log4net;
using MT.Singularity;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.UserManagement;

namespace Mt.Proxaml.Wpf.SetupNodes.MainNode
{
    [Export(typeof(IConfigurable))]
    [Export(typeof(ICustomSetupComponents))]
    [InjectionBehavior(IsSingleton = true)]
    public class CustomSetupComponents : ConfigurationStoreConfigurable<CustomSetupConfiguration>,
        ICustomSetupComponents
    {
        private const string ThisTypeName = nameof(CustomSetupComponents);
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;

        public CustomSetupComponents(
            IConfigurationStore configurationStore,
            ISecurityService securityService,
            CompositionContainer compositionContainer)
            : base(configurationStore, securityService, compositionContainer)
        {
            Logger.TraceEx(ThisTypeName);
            FriendlyName = nameof(CustomSetupComponents).InsertSeparatorsInCamelCase();

            ConfigurationSelector = nameof(CustomSetupConfiguration);
        }

        /// <inheritdoc cref="ConfigurationStoreConfigurable{TConfiguration}" />
        public override string FriendlyName { get; }

        /// <inheritdoc cref="ConfigurationStoreConfigurable{TConfiguration}" />
        public override string ConfigurationSelector { get; }
    }
}