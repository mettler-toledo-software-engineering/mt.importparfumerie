﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Mt.Proxaml.Wpf.SetupNodes.SubNode1;
using Mt.Proxaml.Wpf.SetupNodes.SubNode2;
using MT.Singularity.Composition;
using MT.Singularity.Platform.UI.Shell.Setup;
using MT.Singularity.Platform.UI.Shell.Setup.MenuItems;

namespace Mt.Proxaml.Wpf.SetupNodes.MainNode
{
    [Export(typeof(ICustomerSetupMenuItem))]
    public class CustomSetupMenuItem : ICustomerSetupMenuItem
    {
        private readonly ISetupMenuContext _setupMenuContext;

        public CustomSetupMenuItem(ISetupMenuContext setupMenuContext)
        {
            _setupMenuContext = setupMenuContext ?? throw new ArgumentNullException(nameof(setupMenuContext));
        }

        public string Title => "Konfiguration";

        public Type ViewModelType => typeof(CustomSetupNodeAViewModel);

        public object ViewModelData { get; }

        public bool HasDefaultDataOnly => false;

        public Task<IEnumerable<ISetupMenuItem>> GetChildrenAsync()
        {
            var children = new List<ISetupMenuItem>
            {
                new CustomSetupMenuItemA(typeof(CustomSetupNodeAViewModel)),
                new DatabaseMenuItem(typeof(DatabaseNodeViewModel))
            };

            return Task.FromResult((IEnumerable<ISetupMenuItem>)children);
        }

        public Task<bool> IsLeafNode()
        {
            return Task.FromResult(false);
        }
    }
}