﻿using MT.Singularity.Platform.Configuration;

namespace Mt.Proxaml.Wpf.SetupNodes.MainNode
{
    public interface ICustomSetupComponents : IConfigurable<CustomSetupConfiguration>
    {
    }
}