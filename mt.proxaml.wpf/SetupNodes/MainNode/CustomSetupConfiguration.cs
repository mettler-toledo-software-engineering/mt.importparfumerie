﻿using System.ComponentModel;
using Mt.Proxaml.Application.Models;
using MT.Singularity.Components;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.Configuration.Attributes;

namespace Mt.Proxaml.Wpf.SetupNodes.MainNode
{
    public class CustomSetupConfiguration : ComponentConfiguration
    {
        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(Ports.Port5)]
        [Enabled("Enabled")]
        public virtual Ports Port
        {
            get => (Ports)GetLocal();
            set => SetLocal(value);
        }

        [Setting(WritePermissions = Permissions.UntilAdministratorId)]
        [DefaultValue(2.0)]
        public virtual double StabilizationTime
        {
            get => (double)GetLocal();
            set => SetLocal(value);
        }

        [Setting(WritePermissions = Permissions.UntilAdministratorId)]
        [DefaultValue(0.5)]
        public virtual double MinimumWeight
        {
            get => (double)GetLocal();
            set => SetLocal(value);
        }

        [Setting(WritePermissions = Permissions.UntilAdministratorId)]
        [DefaultValue(5.0)]
        public virtual double TolerancePercentage
        {
            get => (double)GetLocal();
            set => SetLocal(value);
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [DefaultValue("localhost")]
        public virtual string DataHost
        {
            get => (string)GetLocal();
            set => SetLocal(value);
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [DefaultValue("1521")]
        public virtual string DataPort
        {
            get => (string)GetLocal();
            set => SetLocal(value);
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [DefaultValue("XEPDB1")]
        public virtual string DataServiceName
        {
            get => (string)GetLocal();
            set => SetLocal(value);
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [DefaultValue("W5_PROD")]
        public virtual string UserId
        {
            get => (string)GetLocal();
            set => SetLocal(value);
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [DefaultValue("coop")]
        public virtual string DataPassword
        {
            get => (string)GetLocal();
            set => SetLocal(value);
        }
    }
}