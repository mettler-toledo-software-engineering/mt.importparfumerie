﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Mt.Proxaml.Application.Models;
using Mt.Proxaml.Application.Services.SystemUpdate;
using Mt.Proxaml.Wpf.Commands;
using Mt.Proxaml.Wpf.SetupNodes.MainNode;
using Mt.Proxaml.Wpf.Stores;
using MT.Singularity.Composition;
using MT.Singularity.Platform.UI.Shell.CommonControl;
using MT.Singularity.Platform.UI.Shell.Setup.Pages;
using MT.Singularity.Presentation.UserInteraction;
using MT.Singularity.Translation;

namespace Mt.Proxaml.Wpf.SetupNodes.SubNode1
{
    [Export(typeof(CustomSetupNodeAViewModel))]
    public class CustomSetupNodeAViewModel : ConfigurationPanelBase<ICustomSetupComponents, CustomSetupConfiguration>
    {
        private readonly ICustomSetupComponents _customSetupComponents;

        public CustomSetupNodeAViewModel(
            CompositionContainer compositionContainer, IUserInteraction userInteraction,
            ICustomSetupComponents customSetupComponents, GlobalMessageStore globalMessageStore,
            UserStore userStore) : base(
            compositionContainer, userInteraction)
        {
            _customSetupComponents =
                customSetupComponents ?? throw new ArgumentNullException(nameof(customSetupComponents));

            UpdateSystemCommand = new UpdateSystemCommand(this, compositionContainer.Resolve<ISystemUpdateService>(),
                globalMessageStore, userStore);
        }

        public ICommand UpdateSystemCommand { get; }

        private string _updateResult;

        public string UpdateResult
        {
            get => _updateResult;
            set => SetField(ref _updateResult, value);
        }

        public ConfigurationPropertyEnumSelector<Ports> Port { get; private set; }
        public ConfigurationProperty<double> StabilizationTime { get; private set; }
        public ConfigurationProperty<double> MinimumWeight { get; private set; }
        public ConfigurationProperty<double> TolerancePercentage { get; private set; }

        public override string Caption => null;

        protected override async Task LoadConfigurationAsync(object context)
        {
            Configurable = _customSetupComponents;

            if (Configurable != null)
            {
                Configuration = await Configurable.GetConfigurationToChangeAsync();
                Port = new ConfigurationPropertyEnumSelector<Ports>(this, Configuration, nameof(Configuration.Port),
                    SingularityTranslation.Key.Print);
                StabilizationTime =
                    new ConfigurationProperty<double>(this, Configuration, nameof(Configuration.StabilizationTime));
                MinimumWeight =
                    new ConfigurationProperty<double>(this, Configuration, nameof(Configuration.MinimumWeight));
                TolerancePercentage =
                    new ConfigurationProperty<double>(this, Configuration, nameof(Configuration.TolerancePercentage));
            }
        }
    }
}