﻿using MT.Singularity.Platform.UI.Infrastructure;

namespace Mt.Proxaml.Wpf.SetupNodes.SubNode1
{
    /// <summary>
    ///     Interaction logic for CustomSetupNodeAView.xaml
    /// </summary>
    [SupportedViewModel(typeof(CustomSetupNodeAViewModel))]
    public partial class CustomSetupNodeAView
    {
        internal readonly CustomSetupNodeAViewModel ViewModel;

        public CustomSetupNodeAView(CustomSetupNodeAViewModel viewModel)
        {
            DataContext = ViewModel = viewModel;

            InitializeComponent();
        }
    }
}