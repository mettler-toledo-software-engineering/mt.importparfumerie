﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MT.Singularity.Platform.UI.Shell.Setup;

namespace Mt.Proxaml.Wpf.SetupNodes.SubNode1
{
    public class CustomSetupMenuItemA : ISetupMenuItem
    {
        public CustomSetupMenuItemA(
            Type viewModelType = null,
            object viewModelData = null)
        {
            ViewModelType = viewModelType;
            ViewModelData = viewModelData;
        }

        public string Title => "Generelle Konfiguration";

        public Type ViewModelType { get; }

        public object ViewModelData { get; }

        public bool HasDefaultDataOnly => false;

        public Task<IEnumerable<ISetupMenuItem>> GetChildrenAsync()
        {
            return Task.FromResult(Enumerable.Empty<ISetupMenuItem>());
        }

        public Task<bool> IsLeafNode()
        {
            return Task.FromResult(true);
        }
    }
}