﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MT.Singularity.Platform.UI.Shell.Setup;

namespace Mt.Proxaml.Wpf.SetupNodes.SubNode2
{
    public class DatabaseMenuItem : ISetupMenuItem
    {
        public DatabaseMenuItem(
            Type viewModelType = null,
            object viewModelData = null)
        {
            ViewModelType = viewModelType;
            ViewModelData = viewModelData;
        }

        public Type ViewModelType { get; }

        public object ViewModelData { get; }

        public bool HasDefaultDataOnly => false;
        public string Title => "Datenbank Konfig";

        public Task<IEnumerable<ISetupMenuItem>> GetChildrenAsync()
        {
            return Task.FromResult(Enumerable.Empty<ISetupMenuItem>());
        }

        public Task<bool> IsLeafNode()
        {
            return Task.FromResult(true);
        }
    }
}