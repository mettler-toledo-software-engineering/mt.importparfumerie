﻿using System.Windows;
using MT.Singularity.Platform.UI.Infrastructure;

namespace Mt.Proxaml.Wpf.SetupNodes.SubNode2
{
    /// <summary>
    ///     Interaction logic for CustomSetupNodeBView.xaml
    /// </summary>
    [SupportedViewModel(typeof(DatabaseNodeViewModel))]
    public partial class DatenbankNodeView
    {
        internal readonly DatabaseNodeViewModel ViewModel;

        public DatenbankNodeView(DatabaseNodeViewModel viewModel)
        {
            DataContext = ViewModel = viewModel;

            InitializeComponent();
        }

        private void UIElement_OnGotFocus(object sender, RoutedEventArgs e)
        {
            ResetResult();
        }

        private void ResetResult()
        {
            ViewModel.UpdateResult = string.Empty;
        }
    }
}