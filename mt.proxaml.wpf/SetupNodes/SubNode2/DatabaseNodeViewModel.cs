﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Mt.Proxaml.Wpf.Commands;
using Mt.Proxaml.Wpf.SetupNodes.MainNode;
using Mt.Proxaml.Wpf.Stores;
using MT.Singularity.Composition;
using MT.Singularity.Platform.UI.Shell.CommonControl;
using MT.Singularity.Platform.UI.Shell.Setup.Pages;
using MT.Singularity.Presentation.UserInteraction;

namespace Mt.Proxaml.Wpf.SetupNodes.SubNode2
{
    [Export(typeof(DatabaseNodeViewModel))]
    public class DatabaseNodeViewModel : ConfigurationPanelBase<ICustomSetupComponents, CustomSetupConfiguration>
    {
        private readonly ICustomSetupComponents _customSetupComponents;

        public DatabaseNodeViewModel(
            CompositionContainer compositionContainer, IUserInteraction userInteraction,
            ICustomSetupComponents customSetupComponents, ConnectionStringStore connectionStringStore,
            GlobalMessageStore globalMessageStore, UserStore userStore) :
            base(compositionContainer, userInteraction)
        {
            _customSetupComponents =
                customSetupComponents ?? throw new ArgumentNullException(nameof(customSetupComponents));
            connectionStringStore.DatabaseTestStatusChanged += DatabaseTestStatusChanged;
            TestConnectionString =
                new TestConnectionStringCommand(this, connectionStringStore, globalMessageStore, userStore);
        }

        public ICommand TestConnectionString { get; }

        public ConfigurationProperty<string> DataHost { get; private set; }
        public ConfigurationProperty<string> DataPort { get; private set; }
        public ConfigurationProperty<string> DataServiceName { get; private set; }
        public ConfigurationProperty<string> UserId { get; private set; }
        public ConfigurationProperty<string> DataPassword { get; private set; }

        public string ConnectionString =>
            $"User Id={UserId.Value};Password={DataPassword.Value};Data Source={DataHost.Value}:{DataPort.Value}/{DataServiceName.Value}";

        private string _updateResult;

        public string UpdateResult
        {
            get => _updateResult;
            set => SetField(ref _updateResult, value);
        }

        public override string Caption => null;

        protected override async Task LoadConfigurationAsync(object context)
        {
            Configurable = _customSetupComponents;

            if (Configurable != null)
            {
                Configuration = await Configurable.GetConfigurationToChangeAsync();
                DataHost = new ConfigurationProperty<string>(this, Configuration, nameof(Configuration.DataHost));
                DataPort = new ConfigurationProperty<string>(this, Configuration, nameof(Configuration.DataPort));
                DataServiceName =
                    new ConfigurationProperty<string>(this, Configuration, nameof(Configuration.DataServiceName));
                UserId = new ConfigurationProperty<string>(this, Configuration, nameof(Configuration.UserId));
                DataPassword =
                    new ConfigurationProperty<string>(this, Configuration, nameof(Configuration.DataPassword));
            }
        }

        private void DatabaseTestStatusChanged(string displayStatus)
        {
            UpdateResult = displayStatus;
        }
    }
}