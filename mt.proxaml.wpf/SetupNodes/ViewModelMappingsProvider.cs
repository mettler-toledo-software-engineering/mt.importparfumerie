﻿using MT.Singularity.Composition;
using MT.Singularity.Platform.UI.Infrastructure;

namespace Mt.Proxaml.Wpf.SetupNodes
{
    [Export(typeof(IViewModelMappingsProvider))]
    public class ViewModelMappingsProvider : AttributedViewModelMappingsProviderBase
    {
    }
}