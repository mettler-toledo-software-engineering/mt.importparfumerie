﻿using System;
using System.Windows.Media;
using Mt.Proxaml.Application.Models;
using Mt.Proxaml.Application.Services;
using Mt.Proxaml.Wpf.SetupNodes.MainNode;
using MT.Singularity.Platform.Devices.Scale;

namespace Mt.Proxaml.Wpf.Stores
{
    public class ColorStore
    {
        private readonly CustomSetupStore _customSetupStore;
        private readonly GlobalMessageStore _messageStore;
        private readonly MeasurementService _measurementService;
        private readonly ProductStore _productStore;
        private readonly ScaleStore _scaleStore;
        private readonly ColorService _colorService;
        private double _lastTarget;
        private double _lowerLimit;
        private double _minimumWeight;
        private double _stableWeightDuration;
        private double _upperLimit;
        private bool _productReceived = false;

        public ColorStore(
            GlobalMessageStore globalMessageStore, CustomSetupStore customSetupStore,
            ProductStore productStore, ScaleStore scaleStore, MeasurementService measurementService,
            ColorService colorService)
        {
            _customSetupStore = customSetupStore;
            _customSetupStore.CurrentCustomSetupConfigurationChanged +=
                CustomSetupStoreOnCurrentCustomSetupConfigurationChanged;
            _productStore = productStore;
            _productStore.ProductChanged += ProductStoreOnProductChanged;
            _messageStore = globalMessageStore;
            _scaleStore = scaleStore;
            _scaleStore.WeightChanged += ScaleStoreOnWeightChanged;
            _measurementService = measurementService;
            _measurementService.UpdateCountDownMessage += MeasurementServiceUpdateCountDownMessage;
            _measurementService.MeasurementDone += MeasurementServiceMeasurement;
            _measurementService.BelowMinimumWeight += MeasurementServiceBelowMinimumMeasurement;
            _colorService = colorService;
            _colorService.ColorChanged += ColorServiceOnColorChanged;
            InitializeVariables();
        }

        public event Action ClearDisplayedValues;
        public event Action<Brush> ColorChanged;
        public event Action<double> ToleranceChanged;

        public double TolerancePercentage { get; private set; }

        private void CustomSetupStoreOnCurrentCustomSetupConfigurationChanged(
            string propertyName,
            CustomSetupConfiguration customSetup)
        {
            switch (propertyName)
            {
                case nameof(customSetup.StabilizationTime):
                    _stableWeightDuration = customSetup.StabilizationTime;
                    _measurementService.UpdateParameters(_stableWeightDuration * 1000, _minimumWeight);
                    break;
                case nameof(customSetup.MinimumWeight):
                    _minimumWeight = customSetup.MinimumWeight;
                    _measurementService.UpdateParameters(_stableWeightDuration * 1000, _minimumWeight);
                    break;
                case nameof(customSetup.TolerancePercentage):
                    TolerancePercentage = customSetup.TolerancePercentage;
                    ToleranceChanged?.Invoke(TolerancePercentage);
                    _upperLimit = _lastTarget * (1 + (TolerancePercentage / 100));
                    _lowerLimit = _lastTarget * (1 - (TolerancePercentage / 100));
                    _colorService.UpdateParameters(_lowerLimit, _upperLimit);
                    break;
            }
        }

        private void InitializeVariables()
        {
            _measurementService.UpdateParameters(
                _customSetupStore.CurrentCustomSetupConfiguration.StabilizationTime * 1000,
                _customSetupStore.CurrentCustomSetupConfiguration.MinimumWeight);
            TolerancePercentage = _customSetupStore.CurrentCustomSetupConfiguration.TolerancePercentage;
        }

        private void ProductStoreOnProductChanged(Product product)
        {
            _upperLimit = product.TargetWeight * (1 + (TolerancePercentage / 100));
            _lowerLimit = product.TargetWeight * (1 - (TolerancePercentage / 100));
            _colorService.UpdateParameters(_lowerLimit, _upperLimit);
            _lastTarget = product.TargetWeight;
            _productReceived = true;
        }

        private void ScaleStoreOnWeightChanged(WeightInformation weight)
        {
            if (_productReceived)
            {
                _measurementService.ScaleStoreOnWeightChanged(weight.IsValid, weight.NetWeightString);
            }
        }

        private void MeasurementServiceMeasurement(double netWeight)
        {
            _messageStore.UpdateMessage(
                "Measurement done, please discharge the scale",
                GlobalMessageStore.MessageType.Info);
        }

        private void MeasurementServiceUpdateCountDownMessage(double countDown)
        {
            _messageStore.UpdateCountDownMessage(countDown);
        }

        private void ColorServiceOnColorChanged(Brush obj)
        {
            ColorChanged.Invoke(obj);
        }

        private void MeasurementServiceBelowMinimumMeasurement()
        {
            ClearDisplayedValues?.Invoke();
            _productReceived = false;
        }
    }
}