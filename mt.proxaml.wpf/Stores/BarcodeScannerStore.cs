﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using Mt.Proxaml.Application.Models;
using Mt.Proxaml.Scanner;
using Mt.Proxaml.Wpf.SetupNodes.MainNode;

namespace Mt.Proxaml.Wpf.Stores
{
    /// <summary>
    ///     an example on how to implement a apr331 printer with dependency injection
    ///     the print method only needs to be called by dedicated command
    /// </summary>
    public class BarcodeScannerStore : CustomSetupStore
    {
        private readonly IBarcodeScannerFactory _barcodeScannerFactory;
        private readonly GlobalMessageStore _messageStore;

        private IScanner _barcodeScanner;

        public BarcodeScannerStore(
            ICustomSetupComponents customSetupComponents,
            IBarcodeScannerFactory barcodeScannerFactory,
            GlobalMessageStore messageStore) : base(customSetupComponents)
        {
            _barcodeScannerFactory = barcodeScannerFactory;
            _messageStore = messageStore;
        }

        public event Action<string> BarcodeDataReceived;

        public async Task CloseScannerPortAsync()
        {
            await _barcodeScanner.ClosePortAsync();
        }

        protected override void OnCustomSetupLoaded()
        {
            _barcodeScannerFactory
                .CreateScannerAsync(CurrentCustomSetupConfiguration.Port.ConvertPortsToInt())
                .ContinueWith(HandleException);
        }

        protected override void CurrentCustomSetupConfigurationOnPropertyChanged(
            object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(CurrentCustomSetupConfiguration.Port):
                    PortChanged();
                    break;
            }
        }

        private void HandleException(Task<IScanner> task)
        {
            if (task.Exception != null)
            {
                _messageStore.UpdateMessage(
                    "Barcodescanner Port konnte nicht initialisiert werden",
                    GlobalMessageStore.MessageType.Error, task.Exception);
            }

            _barcodeScanner = task.Result;
            try
            {
                _barcodeScanner.OpenPort();
                Subscribe(_barcodeScanner);
            }
            catch (Exception e)
            {
                _messageStore.UpdateMessage(
                    "Port vom Barcodescanner konnte nicht geöffnet werden",
                    GlobalMessageStore.MessageType.Error, e);
            }
        }

        private void PortChanged()
        {
            try
            {
                _barcodeScanner.ClosePortAsync();
                _barcodeScannerFactory
                    .CreateScannerAsync(CurrentCustomSetupConfiguration.Port.ConvertPortsToInt())
                    .ContinueWith(HandleException);
            }
            catch (Exception e)
            {
                _messageStore.UpdateMessage(
                    "Port vom Barcodescanner konnte nicht geschlossen werden",
                    GlobalMessageStore.MessageType.Error, e);
            }
        }

        private void Subscribe(IScanner barcodeScanner)
        {
            if (_barcodeScanner == null)
            {
                _barcodeScanner = barcodeScanner;
                _barcodeScanner.BarcodeDataReceived += BarcodeScanner_BarcodeDataReceived;
            }
            else
            {
                _barcodeScanner.BarcodeDataReceived -= BarcodeScanner_BarcodeDataReceived;
                _barcodeScanner = barcodeScanner;
                _barcodeScanner.BarcodeDataReceived += BarcodeScanner_BarcodeDataReceived;
            }
        }

        private void BarcodeScanner_BarcodeDataReceived(string obj)
        {
            BarcodeDataReceived?.Invoke(obj);
        }
    }
}