﻿using System;
using System.Threading.Tasks;
using System.Windows;
using Mt.Proxaml.Application.Infrastructure;
using MT.Singularity.Platform.Application.Value;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.ValueSystem;

namespace Mt.Proxaml.Wpf.Stores
{
    /// <summary>
    ///     handles the scale communication and provides methods for commands
    ///     can be expanded if more methods are necessary
    /// </summary>
    public class ScaleStore
    {
        private readonly LanguageStore _languageStore;
        private readonly GlobalMessageStore _messageStore;
        private readonly IScaleService _scaleService;
        private ResourceDictionary _dictionary;
        private WeightInformation _weightInformation;

        public ScaleStore(
            IScaleService scaleService, IApplicationValueService applicationValueService,
            GlobalMessageStore messageStore, LanguageStore languageStore)
        {
            _scaleService = scaleService;
            _messageStore = messageStore;
            _languageStore = languageStore;
            applicationValueService.GetApplicationValueObservableAsync().ContinueWith(HandleException);
            _scaleService.SelectedScaleChanged += ScaleServiceOnSelectedScaleChanged;
            _languageStore.LanguageChanged += LanguageStoreOnLanguageChanged;
            _dictionary = _languageStore.CurrentLanguageDictionary;
        }

        public event Action<WeightInformation> WeightChanged;

        public KnownUnit Unit => _weightInformation.Unit;

        private IScale SelectedScale { get; set; }

        public Task<WeightInformation> GetStableWeightAsync(UnitType unitType, int timeout)
        {
            return SelectedScale.GetStableWeightAsync(unitType, timeout);
        }

        public Task<PresetTareResult> PresetTareScaleAsync(string tareValue, KnownUnit unit)
        {
            return SelectedScale.PresetTareWithResultAsync(tareValue, unit);
        }

        // the following methods are already handled by default buttons, but can be included if complex scale handling is necessary
        public Task<WeightState> TareScaleAsync()
        {
            return SelectedScale.TareAsync();
        }

        public Task<WeightState> ClearTareAsync()
        {
            return SelectedScale.ClearTareAsync();
        }

        public Task<WeightState> ZeroScaleAsync()
        {
            return SelectedScale.ZeroAsync();
        }

        public Task<bool> SwitchScaleAsync()
        {
            return _scaleService.SwitchSelectedScaleAsync();
        }

        private void LanguageStoreOnLanguageChanged()
        {
            _dictionary = _languageStore.CurrentLanguageDictionary;
        }

        private void ScaleServiceOnSelectedScaleChanged()
        {
            SelectedScale = _scaleService.SelectedScale;
        }

        private void HandleException(Task<IObservable<ApplicationValue>> task)
        {
            if (task.Exception != null)
            {
                string text = (string)_dictionary[Globals.ERRSCALE1];

                _messageStore.UpdateMessage(text, GlobalMessageStore.MessageType.Error, task.Exception);
                return;
            }

            task.Result.Subscribe(ApplicationValueGenerator);
        }

        private void ApplicationValueGenerator(ApplicationValue applicationValue)
        {
            _weightInformation = applicationValue.WeightInformation;
            WeightChanged?.Invoke(applicationValue.WeightInformation);
        }
    }
}