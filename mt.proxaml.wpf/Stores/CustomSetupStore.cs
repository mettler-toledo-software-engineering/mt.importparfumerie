﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using Mt.Proxaml.Wpf.SetupNodes.MainNode;

namespace Mt.Proxaml.Wpf.Stores
{
    /// <summary>
    ///     this store provides the current setup configuration of the terminal and triggers events if the setup changes
    /// </summary>
    public class CustomSetupStore
    {
        public CustomSetupStore(ICustomSetupComponents customSetupComponents)
        {
            customSetupComponents.GetConfigurationAsync().ContinueWith(HandleException);
        }

        public event Action<string, CustomSetupConfiguration> CurrentCustomSetupConfigurationChanged;

        public CustomSetupConfiguration CurrentCustomSetupConfiguration { get; private set; }

        protected virtual void OnCustomSetupLoaded()
        {
        }

        protected virtual void CurrentCustomSetupConfigurationOnPropertyChanged(
            object sender, PropertyChangedEventArgs e)
        {
            CurrentCustomSetupConfigurationChanged?.Invoke(e.PropertyName, CurrentCustomSetupConfiguration);
        }

        private void HandleException(Task<CustomSetupConfiguration> task)
        {
            if (task.Exception != null)
            {
                return;
            }

            CurrentCustomSetupConfiguration = task.Result;
            CurrentCustomSetupConfiguration.PropertyChanged += CurrentCustomSetupConfigurationOnPropertyChanged;
            OnCustomSetupLoaded();
        }
    }
}