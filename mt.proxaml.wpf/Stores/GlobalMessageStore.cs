﻿using System;
using log4net;
using MT.Singularity.Composition;
using MT.Singularity.Logging;

namespace Mt.Proxaml.Wpf.Stores
{
    /// <summary>
    ///     handling of all messages that need to be displayed to the user
    ///     IEnginginemessageService can be used to add the message to the info area of singularity as well
    /// </summary>
    [Export(typeof(GlobalMessageStore))]
    [InjectionBehavior(IsSingleton = true)]
    public class GlobalMessageStore
    {
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;

        public GlobalMessageStore()
        {
            CurrentMessage = "Applikation Startet...";
            CurrentMessageType = MessageType.Info;
        }

        public event Action<double> CountDown;

        public event Action CurrentMessageChanged;

        public enum MessageType
        {
            Success,
            Info,
            Error,
            None
        }

        public int MessageDuration { get; private set; } = 5000;

        public string CurrentMessage { get; private set; }

        public MessageType CurrentMessageType { get; private set; }

        public void UpdateMessage(string message, MessageType messageType, Exception exception = null)
        {
            MessageDuration = 5000;

            Update(message, messageType, exception);
        }

        public void UpdateCountDownMessage(double countDown)
        {
            string message = $"Zeit bis zur Stabilisierunng und Rückmeldung {countDown} Sekunden";
            Update(message, MessageType.Info);
        }

        public void UpdateMessage(
            string message, MessageType messageType, int messageDuration,
            Exception exception = null)
        {
            MessageDuration = messageDuration * 1000;

            Update(message, messageType, exception);
        }

        public void UpdateCountDown(double countdown)
        {
            CountDown?.Invoke(countdown);
        }

        private void Update(string message, MessageType messageType, Exception exception = null)
        {
            CurrentMessage = message;
            CurrentMessageType = messageType;
            CurrentMessageChanged?.Invoke();

            switch (CurrentMessageType)
            {
                case MessageType.Error:
                    Logger.Error(message, exception);
                    break;
                case MessageType.Info:
                    Logger.Info(message);
                    break;
            }
        }
    }
}