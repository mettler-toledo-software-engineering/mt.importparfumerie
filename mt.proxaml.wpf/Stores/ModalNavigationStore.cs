﻿using System;
using Mt.Proxaml.Wpf.ViewModels;
using MT.Singularity.Composition;

namespace Mt.Proxaml.Wpf.Stores
{
    /// <summary>
    ///     handles the navigation for the current modal that needs to be displayed
    /// </summary>
    [Export(typeof(ModalNavigationStore))]
    [InjectionBehavior(IsSingleton = true)]
    public class ModalNavigationStore
    {
        private SimpleViewModelBase _currentViewModel;

        public event Action CurrentViewModelChanged;

        /// <summary>
        ///     disposes the old viewmodel before setting the new one during navigation, in order to free all eventhandlers
        /// </summary>
        public SimpleViewModelBase CurrentViewModel
        {
            get => _currentViewModel;
            set
            {
                _currentViewModel?.Dispose();
                _currentViewModel = value;
                OnCurrentViewModelChanged();
            }
        }

        public bool ModalIsOpen => CurrentViewModel != null;

        /// <summary>
        ///     sets the CurrentViewModel to Null in order to ClosePortAsync the Modal
        /// </summary>
        public void Close()
        {
            CurrentViewModel = null;
        }

        private void OnCurrentViewModelChanged()
        {
            CurrentViewModelChanged?.Invoke();
        }
    }
}