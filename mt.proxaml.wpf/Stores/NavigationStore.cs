﻿using System;
using Mt.Proxaml.Wpf.ViewModels;
using MT.Singularity.Composition;

namespace Mt.Proxaml.Wpf.Stores
{
    /// <summary>
    ///     handles the current viewmodel that needs to be displayed and informs the homescreen viewmodel if navigation happens
    /// </summary>
    [Export(typeof(NavigationStore))]
    [InjectionBehavior(IsSingleton = true)]
    public class NavigationStore
    {
        private SimpleViewModelBase _currentViewModel;

        public event Action CurrentViewModelChanged;

        /// <summary>
        ///     disposes the old viewmodel before setting the new one during navigation, in order to free all eventhandlers
        /// </summary>
        public SimpleViewModelBase CurrentViewModel
        {
            get => _currentViewModel;
            set
            {
                _currentViewModel?.Dispose();
                _currentViewModel = value;
                OnCurrentViewModelChanged();
            }
        }

        private void OnCurrentViewModelChanged()
        {
            CurrentViewModelChanged?.Invoke();
        }
    }
}