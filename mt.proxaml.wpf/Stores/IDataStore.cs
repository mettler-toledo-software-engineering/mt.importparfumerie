﻿using System.Threading.Tasks;

namespace Mt.Proxaml.Wpf.Stores
{
    /// <summary>
    ///     example for a viewmodel generalization, where different viewmodels might need a command to load Data
    /// </summary>
    public interface IDataStore
    {
        Task LoadDataAsync();
    }
}