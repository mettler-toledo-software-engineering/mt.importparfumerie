﻿using System;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.UserManagement;

namespace Mt.Proxaml.Wpf.Stores
{
    /// <summary>
    ///     an Example on how to manage permissions and currently logged in users.
    /// </summary>
    public class UserStore
    {
        private Permission _lastPermission;

        public UserStore(ISecurityService securityService)
        {
            securityService.CurrentUserChanged += SecurityServiceOnCurrentUserChanged;
            IsAdmin = Permissions.IsAdministrator(securityService.CurrentPermission);
            _lastPermission = securityService.CurrentPermission;
        }

        public event Action AdminRightsRemoved;
        public event Action AdminRightsGiven;

        public bool IsAdmin { get; private set; }

        private User CurrentUser { get; set; }

        private void SecurityServiceOnCurrentUserChanged(User user)
        {
            CurrentUser = user;
            if (Permissions.IsAdministrator(CurrentUser.Permission))
            {
                IsAdmin = true;
                AdminRightsGiven?.Invoke();
            }
            else if (Permissions.IsAdministrator(_lastPermission))
            {
                IsAdmin = false;
                AdminRightsRemoved?.Invoke();
            }

            _lastPermission = CurrentUser.Permission;
        }
    }
}