﻿using System;
using System.ComponentModel;
using System.Timers;
using Microsoft.EntityFrameworkCore;
using Mt.Proxaml.Application.Infrastructure;
using Mt.Proxaml.Application.Services;
using Mt.Proxaml.persistence.Context;
using Mt.Proxaml.Wpf.SetupNodes.MainNode;

namespace Mt.Proxaml.Wpf.Stores
{
    public class ConnectionStringStore : CustomSetupStore
    {
        public Action<string> DatabaseStatusChanged;
        public Action<string> DatabaseTestStatusChanged;
        public Action ConnectionStringChanged;

        private readonly ModelContextFactory _contextFactory;
        private readonly GlobalMessageStore _messageStore;

        public ConnectionStringStore(
            ICustomSetupComponents customSetupComponents,
            GlobalMessageStore messageStore,
            ModelContextFactory contextFactory) : base(customSetupComponents)
        {
            _messageStore = messageStore;
            _contextFactory = contextFactory;

            Timer checkDbConnectionTimer = new Timer
            {
                AutoReset = true,
                Interval = 30000,
                Enabled = true
            };
            checkDbConnectionTimer.Elapsed += CheckDbConnectionTimerOnElapsed;
        }

        public string DatabaseInfoDisplay => CurrentCustomSetupConfiguration?.DataHost != null && CurrentCustomSetupConfiguration?.DataPort != null ? $"{CurrentCustomSetupConfiguration.DataHost} {CurrentCustomSetupConfiguration.DataPort}" : string.Empty;

        public void TestCurrentConnectionString(bool updateConnectionString)
        {
            string currentConnectionString =
                $"User Id={CurrentCustomSetupConfiguration.UserId};Password={CurrentCustomSetupConfiguration.DataPassword};Data Source={CurrentCustomSetupConfiguration.DataHost}:{CurrentCustomSetupConfiguration.DataPort}/{CurrentCustomSetupConfiguration.DataServiceName}";
            TestConnectionString(updateConnectionString, currentConnectionString);
        }

        public void TestConnectionString(bool updateConnectionString, string currentConnectionString)
        {
            try
            {
                TestConnectionStringService.ConnectionStringIsValid(currentConnectionString);
            }
            catch (Exception e)
            {
                _messageStore.UpdateMessage(
                    "Falsche Datenbank Informationen, Verbindung nicht möglich",
                    GlobalMessageStore.MessageType.Error, e);
                DatabaseStatusChanged?.Invoke("Nicht Verbunden");
                return;
            }

            DatabaseStatusChanged?.Invoke("Verbunden");
            if (!updateConnectionString)
            {
                return;
            }

            UpdateDbContext(currentConnectionString);
            WriteDbConfigurationToFile();
        }

        public void UpdateDbContext(string connectionString)
        {
            _contextFactory.ConfigureDbContext = o => o.UseOracle(connectionString);
        }

        public void TestConnectionStringButton(string connection)
        {
            try
            {
                TestConnectionStringService.ConnectionStringIsValid(connection);
                DatabaseTestStatusChanged?.Invoke("Verbindung erfolgreich");
            }
            catch
            {
                DatabaseTestStatusChanged?.Invoke("Verbindung fehlgeschlagen");
            }
        }

        protected override void CurrentCustomSetupConfigurationOnPropertyChanged(
            object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(CurrentCustomSetupConfiguration.DataHost):
                case nameof(CurrentCustomSetupConfiguration.DataPassword):
                case nameof(CurrentCustomSetupConfiguration.DataPort):
                case nameof(CurrentCustomSetupConfiguration.DataServiceName):
                case nameof(CurrentCustomSetupConfiguration.UserId):
                    TestCurrentConnectionString(true);
                    ConnectionStringChanged?.Invoke();
                    break;
            }
        }

        protected override void OnCustomSetupLoaded()
        {
            ConnectionStringChanged?.Invoke();
        }

        private void WriteDbConfigurationToFile()
        {
            ConnectionString connectionString = new ConnectionString
            {
                UserId = CurrentCustomSetupConfiguration.UserId,
                Password = CurrentCustomSetupConfiguration.DataPassword,
                Host = CurrentCustomSetupConfiguration.DataHost,
                Port = CurrentCustomSetupConfiguration.DataPort,
                ServiceName = CurrentCustomSetupConfiguration.DataServiceName
            };
            try
            {
                SerializationService.ConfigurationToJson(connectionString);
            }
            catch (Exception e)
            {
                _messageStore.UpdateMessage(
                    "Datenbankkonfiguration konnte nicht in die Datei geschrieben werden",
                    GlobalMessageStore.MessageType.Error, e);
            }
        }

        private void CheckDbConnectionTimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            TestCurrentConnectionString(false);
        }
    }
}