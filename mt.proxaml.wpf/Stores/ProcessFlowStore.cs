﻿using System;
using System.Windows.Media;
using Mt.Proxaml.Application.Models;
using Mt.Proxaml.Application.Services;
using MT.Singularity.Platform.Devices.Scale;

namespace Mt.Proxaml.Wpf.Stores
{
    public class ProcessFlowStore
    {
        private readonly GlobalMessageStore _messageStore;
        private readonly IMeasurementService _measurementService;
        private bool _productReceived = false;

        public ProcessFlowStore(
            GlobalMessageStore globalMessageStore, ProductStore productStore, ScaleStore scaleStore,
            IMeasurementService measurementService,
            IColorService colorService)
        {
            productStore.ProductReceived += ProductStoreOnProductReceived;
            _messageStore = globalMessageStore;
            scaleStore.WeightChanged += ScaleStoreOnWeightChanged;
            _measurementService = measurementService;
            _measurementService.UpdateCountDownMessage += MeasurementServiceUpdateCountDownMessage;
            _measurementService.MeasurementDone += MeasurementDone;
            _measurementService.BelowMinimumWeight += BelowMinimumWeight;
            colorService.ColorChanged += ColorServiceOnColorChanged;
        }

        public event Action ClearDisplayedValues;
        public event Action<Brush> ColorChanged;

        private void ProductStoreOnProductReceived(Product product)
        {
            _productReceived = true;
        }

        private void ScaleStoreOnWeightChanged(WeightInformation weight)
        {
            if (_productReceived)
            {
                _measurementService.ScaleStoreOnWeightChanged(weight.IsValid, weight.NetWeightString);
            }
        }

        private void MeasurementDone(double netWeight)
        {
            _messageStore.UpdateMessage(
                "Messung beendet, bitte die Waage entladen",
                GlobalMessageStore.MessageType.Info);
        }

        private void MeasurementServiceUpdateCountDownMessage(double countDown)
        {
            _messageStore.UpdateCountDownMessage(countDown);
        }

        private void ColorServiceOnColorChanged(Brush obj)
        {
            ColorChanged?.Invoke(obj);
        }

        private void BelowMinimumWeight()
        {
            ClearDisplayedValues?.Invoke();
            _productReceived = false;
        }
    }
}