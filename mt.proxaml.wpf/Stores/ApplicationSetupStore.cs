﻿using System;
using System.ComponentModel;
using Mt.Proxaml.Application.Services;
using Mt.Proxaml.Wpf.SetupNodes.MainNode;

namespace Mt.Proxaml.Wpf.Stores
{
    public class ApplicationSetupStore : CustomSetupStore
    {
        private readonly IMeasurementService _measurementService;
        private readonly IColorService _colorService;

        public ApplicationSetupStore(
            ICustomSetupComponents customSetupComponents, IMeasurementService measurementService,
            IColorService colorService) : base(customSetupComponents)
        {
            _measurementService = measurementService;
            _colorService = colorService;
        }

        public event Action<double> ToleranceChanged;

        public double LastTarget { get; set; }

        protected override void CurrentCustomSetupConfigurationOnPropertyChanged(
            object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(CurrentCustomSetupConfiguration.StabilizationTime):
                case nameof(CurrentCustomSetupConfiguration.MinimumWeight):
                    _measurementService.UpdateParameters(
                        CurrentCustomSetupConfiguration.StabilizationTime * 1000,
                        CurrentCustomSetupConfiguration.MinimumWeight);
                    break;
                case nameof(CurrentCustomSetupConfiguration.TolerancePercentage):
                    ToleranceChanged?.Invoke(CurrentCustomSetupConfiguration.TolerancePercentage);
                    double upperLimit = LastTarget * (1 + (CurrentCustomSetupConfiguration.TolerancePercentage / 100));
                    double lowerLimit = LastTarget * (1 - (CurrentCustomSetupConfiguration.TolerancePercentage / 100));
                    _colorService.UpdateParameters(lowerLimit, upperLimit);
                    break;
            }
        }

        protected override void OnCustomSetupLoaded()
        {
            _measurementService.UpdateParameters(
                CurrentCustomSetupConfiguration.StabilizationTime * 1000,
                CurrentCustomSetupConfiguration.MinimumWeight);
            ToleranceChanged?.Invoke(CurrentCustomSetupConfiguration.TolerancePercentage);
        }
    }
}