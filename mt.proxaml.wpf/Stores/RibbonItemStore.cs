﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Mt.Proxaml.Application.Infrastructure;
using MT.Singularity.Platform.UI.Component.Ribbon;
using MT.Singularity.Platform.UI.Shell.Ribbon;

namespace Mt.Proxaml.Wpf.Stores
{
    /// <summary>
    ///     holds all default buttons so they can be called and referenced by all viewmodels
    /// </summary>
    public class RibbonItemStore
    {
        private readonly IRibbonItemsProvider _ribbonItemsProvider;

        public RibbonItemStore(IRibbonItemsProvider ribbonItemsProvider)
        {
            _ribbonItemsProvider = ribbonItemsProvider;

            CreateRibbonItemsAsync().ContinueWith(HandleException);
        }

        public event Action<List<IRibbonItem>> DefaultRibbonItemsLoaded;

        public IRibbonItem ZeroKey { get; private set; }
        public IRibbonItem TareKey { get; private set; }
        public IRibbonItem ClearKey { get; private set; }
        public IRibbonItem PlaceHolderKey { get; } = new RibbonItem(Globals.PlaceHolderKeyId);
        private IRibbonItem PrintKey { get; set; }
        private IRibbonItem PresetTareKey { get; set; }
        private IRibbonItem SwitchScaleKey { get; set; }
        private IRibbonItem SwitchUnitKey { get; set; }
        private IRibbonItem AlibiKey { get; set; }
        private IRibbonItem HighResKey { get; set; }

        private List<IRibbonItem> DefaultRibbonItems { get; } = new List<IRibbonItem>();

        private void HandleException(Task task)
        {
            if (task.Exception != null)
            {
                throw new Exception();
            }
        }

        private async Task CreateRibbonItemsAsync()
        {
            ZeroKey = await _ribbonItemsProvider.CreateItemAsync(Globals.ZeroItemId);
            TareKey = await _ribbonItemsProvider.CreateItemAsync(Globals.TareItemId);
            ClearKey = await _ribbonItemsProvider.CreateItemAsync(Globals.ClearItemId);

            PrintKey = await _ribbonItemsProvider.CreateItemAsync(Globals.PrintItemId);
            PresetTareKey = await _ribbonItemsProvider.CreateItemAsync(Globals.PresetTareItemId);
            SwitchScaleKey =
                await _ribbonItemsProvider.CreateItemAsync(Globals.SwitchScaleItemId);

            SwitchUnitKey = await _ribbonItemsProvider.CreateItemAsync(Globals.SwitchUnitItemId);
            AlibiKey = await _ribbonItemsProvider.CreateItemAsync(Globals.AlibiTableItemId);
            HighResKey = await _ribbonItemsProvider.CreateItemAsync(Globals.ShowHighResolutionItemId);

            DefaultRibbonItems.Add(ZeroKey);
            DefaultRibbonItems.Add(TareKey);
            DefaultRibbonItems.Add(ClearKey);

            DefaultRibbonItems.Add(PrintKey);
            DefaultRibbonItems.Add(PresetTareKey);
            DefaultRibbonItems.Add(SwitchScaleKey);

            DefaultRibbonItems.Add(SwitchUnitKey);
            DefaultRibbonItems.Add(AlibiKey);
            DefaultRibbonItems.Add(HighResKey);

            DefaultRibbonItemsLoaded?.Invoke(DefaultRibbonItems);
        }
    }
}