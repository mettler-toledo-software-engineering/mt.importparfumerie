﻿using System;
using Mt.Proxaml.Application.Models;
using Mt.Proxaml.Application.Services;
using Mt.Proxaml.Application.Services.DataAccess;
using MT.Singularity.Platform.ValueSystem;
using Unit = Mt.Proxaml.Application.Models.Unit;

namespace Mt.Proxaml.Wpf.Stores
{
    /// <summary>
    ///     an example on how to implement a apr331 printer with dependency injection
    ///     the print method only needs to be called by dedicated command
    /// </summary>
    public class ProductStore
    {
        private readonly GlobalMessageStore _messageStore;
        private readonly ScaleStore _scaleStore;
        private readonly ApplicationSetupStore _applicationSetupStore;
        private readonly IBarcodeScannerService _scannerService;
        private readonly IColorService _colorService;
        private readonly IStockobjectbundleService _stockObjectDataService;

        public ProductStore(
            GlobalMessageStore messageStore, ScaleStore scaleStore,
            BarcodeScannerStore barcodeScannerStore, IStockobjectbundleService stockObjectDataService,
            IColorService colorService, ApplicationSetupStore applicationSetupStore, IBarcodeScannerService scannerService)
        {
            _scaleStore = scaleStore;
            _messageStore = messageStore;
            _colorService = colorService;
            barcodeScannerStore.BarcodeDataReceived += BarcodeDataReceived;
            _stockObjectDataService = stockObjectDataService;
            _applicationSetupStore = applicationSetupStore;
            _scannerService = scannerService;
        }

        public event Action<Product> ProductReceived;

        private async void BarcodeDataReceived(string barcode)
        {
            if (!_scannerService.BarcodeSanityCheck(barcode))
            {
                _messageStore.UpdateMessage("Ungültiger Barcode gescannt, beginnt nicht mit ]C100", GlobalMessageStore.MessageType.Error);
                return;
            }

            _messageStore.UpdateMessage("Barcode gescannt", GlobalMessageStore.MessageType.Success);
            string rawBarcodeNoFncOrAi = _scannerService.GetRawBarcode(barcode);
            try
            {
                WeightEntry stockObject = await _stockObjectDataService.GetValuesForBarcode(rawBarcodeNoFncOrAi);
                if (stockObject == null)
                {
                    _messageStore.UpdateMessage(
                        "Kein Eintrag in der Datenbank für den gescannten Barcode gefunden",
                        GlobalMessageStore.MessageType.Error);
                    return;
                }

                Product currentProduct = ProductFromDbService.GetProductFromWeightEntry(stockObject);

                if (SanityCheckPassed(currentProduct))
                {
                    ProductReceived?.Invoke(currentProduct);
                    UpdateColorService(currentProduct);
                    _applicationSetupStore.LastTarget = currentProduct.TargetWeight;
                }
                else
                {
                    _messageStore.UpdateMessage(
                        $"Einheit des Datenbankeintrages {currentProduct?.Unit} ist nicht kg",
                        GlobalMessageStore.MessageType.Error);
                }
            }
            catch (Exception ex)
            {
                _messageStore.UpdateMessage(
                    "Es konnten keine Daten für den Barcode aus der Datenbank gelesen werden",
                    GlobalMessageStore.MessageType.Error, ex);
            }
        }

        private void UpdateColorService(Product currentProduct)
        {
            double upperLimit = currentProduct.TargetWeight * (1 + (_applicationSetupStore.CurrentCustomSetupConfiguration.TolerancePercentage / 100));
            double lowerLimit = currentProduct.TargetWeight * (1 - (_applicationSetupStore.CurrentCustomSetupConfiguration.TolerancePercentage / 100));
            _colorService.UpdateParameters(lowerLimit, upperLimit);
        }

        private bool SanityCheckPassed(Product product)
        {
            return product.Unit == Unit.Kilogram && _scaleStore.Unit == KnownUnit.Kilogram;
        }
    }
}