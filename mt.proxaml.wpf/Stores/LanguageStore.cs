﻿// ReSharper disable once RedundantUsingDirective

using System;
using System.Windows;
using MT.Singularity.Composition;
using MT.Singularity.Localization;
using static Mt.Proxaml.Application.Infrastructure.Globals;

namespace Mt.Proxaml.Wpf.Stores
{
    /// <summary>
    ///     handles language changes and sets the necessary dictionary when the language changes
    ///     provides events for viewmodels to update their language dependent properties
    /// </summary>
    public class LanguageStore
    {
        private readonly ResourceDictionary _englishDictionary = new ResourceDictionary
            { Source = new Uri(@"..\Languages\LanguageEN.xaml", UriKind.Relative) };

        private readonly ResourceDictionary _frenchDictionary = new ResourceDictionary
            { Source = new Uri(@"..\Languages\LanguageFR.xaml", UriKind.Relative) };

        private readonly ResourceDictionary _germanDictionary = new ResourceDictionary
            { Source = new Uri(@"..\Languages\LanguageDE.xaml", UriKind.Relative) };

        private readonly ResourceDictionary _italianDictionary = new ResourceDictionary
            { Source = new Uri(@"..\Languages\LanguageIT.xaml", UriKind.Relative) };

        private readonly ResourceDictionary _spanishDictionary = new ResourceDictionary
            { Source = new Uri(@"..\Languages\LanguageES.xaml", UriKind.Relative) };

        public LanguageStore()
        {
            LocalizationManager.Instance.LanguageChanged += Instance_LanguageChanged;
            SetLanguage();
        }

        public event Action LanguageChanged;

        public ResourceDictionary CurrentLanguageDictionary { get; private set; }

        private void Instance_LanguageChanged(object sender, EventArgs e)
        {
            SetLanguage();
        }

        private void SetLanguage()
        {
            // remove existing language dictionary from application resources
            if (CurrentLanguageDictionary != null)
            {
                System.Windows.Application.Current.Resources.MergedDictionaries.Remove(CurrentLanguageDictionary);
            }

            switch (LocalizationManager.Instance.CurrentLanguage)
            {
                case "en-EN":
                case "en-US":
                    CurrentLanguageDictionary = _englishDictionary;
                    break;
                case "it":
                    CurrentLanguageDictionary = _italianDictionary;
                    break;
                case "fr-FR":
                    CurrentLanguageDictionary = _frenchDictionary;
                    break;
                case "es-ES":
                    CurrentLanguageDictionary = _spanishDictionary;
                    break;
                case "de-DE":
                default:
                    CurrentLanguageDictionary = _germanDictionary;
                    break;
            }

            // add new language dictionary to application resources
            System.Windows.Application.Current.Resources.MergedDictionaries.Add(CurrentLanguageDictionary);
            LanguageChanged?.Invoke();
        }
    }
}