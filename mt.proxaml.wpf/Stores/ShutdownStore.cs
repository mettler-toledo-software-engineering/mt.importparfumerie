﻿using System;

namespace Mt.Proxaml.Wpf.Stores
{
    public class ShutdownStore
    {
        public event Action ShutDownRequested;

        public void ShutDown()
        {
            ShutDownRequested?.Invoke();
        }
    }
}