﻿using MT.Singularity.Composition;

namespace Mt.Proxaml.Wpf.CompositionContainerExtensions
{
    public static class AddViewsToCompositionContainerExtension
    {
        public static CompositionContainer AddViewsToCompositionContainer(this CompositionContainer container)
        {
            return container;
        }
    }
}