﻿using Mt.Proxaml.Application.Services;
using Mt.Proxaml.Application.Services.DataAccess;
using Mt.Proxaml.Application.Services.SystemUpdate;
using Mt.Proxaml.persistence.Context;
using Mt.Proxaml.persistence.Services;
using Mt.Proxaml.Scanner;
using Mt.Proxaml.Wpf.Services;
using Mt.Proxaml.Wpf.Stores;
using Mt.Proxaml.Wpf.ViewModels;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Devices;

namespace Mt.Proxaml.Wpf.CompositionContainerExtensions
{
    public static class AddServicesToCompositionContainerExtension
    {
        public static CompositionContainer AddServicesToCompositionContainer(this CompositionContainer container)
        {
            container.AddInstance(CreateFirstNavigationService(container));
            container.AddInstance<ISystemUpdateService>(new SystemUpdateService());
            container.AddInstance<IBarcodeScannerService>(new BarcodeScannerService());
            container.AddInstance<IStockobjectbundleService>(
                new StockobjectbundleDataService(container.Resolve<ModelContextFactory>()));
            container.AddInstance(new CloseModalNavigationService(container.Resolve<ModalNavigationStore>()));

            container.AddInstance<IBarcodeScannerFactory>(
                new BarcodeScannerFactory(container.Resolve<IInterfaceService>()));
            container.AddInstance<IMeasurementService>(new MeasurementService());
            container.AddInstance<IColorService>(new ColorService(container.Resolve<IMeasurementService>()));

            return container;
        }

        public static INavigationService CreateModalNavigationService(IObjectResolver container)
        {
            return new ModalNavigationService<ModalViewModel>(
                container.Resolve<ModalNavigationStore>(), container.Resolve<ModalViewModel>);
        }

        private static INavigationService CreateFirstNavigationService(IObjectResolver container)
        {
            return new NavigationService<FirstViewModel>(
                container.Resolve<NavigationStore>(),
                container.Resolve<FirstViewModel>);
        }
    }
}