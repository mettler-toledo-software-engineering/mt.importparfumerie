﻿using Mt.Proxaml.Application.Services;
using Mt.Proxaml.Application.Services.DataAccess;
using Mt.Proxaml.persistence.Context;
using Mt.Proxaml.Scanner;
using Mt.Proxaml.Wpf.SetupNodes.MainNode;
using Mt.Proxaml.Wpf.Stores;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Application.Value;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.UI.Shell.Ribbon;
using MT.Singularity.Platform.UserManagement;

namespace Mt.Proxaml.Wpf.CompositionContainerExtensions
{
    public static class AddStoresToCompositionContainerExtension
    {
        public static CompositionContainer AddStoresToCompositionContainer(this CompositionContainer container)
        {
            container.AddInstance(new LanguageStore());
            container.AddInstance(new UserStore(
                container.Resolve<ISecurityService>()));
            container.AddInstance(new CustomSetupStore(container.Resolve<ICustomSetupComponents>()));
            container.AddInstance(new ApplicationSetupStore(
                container.Resolve<ICustomSetupComponents>(),
                container.Resolve<IMeasurementService>(), container.Resolve<IColorService>()));
            container.AddInstance(new RibbonItemStore(
                container.Resolve<IRibbonItemsProvider>()));
            container.AddInstance(new ScaleStore(
                container.Resolve<IScaleService>(),
                container.Resolve<IApplicationValueService>(),
                container.Resolve<GlobalMessageStore>(),
                container.Resolve<LanguageStore>()));
            container.AddInstance(new BarcodeScannerStore(
                container.Resolve<ICustomSetupComponents>(),
                container.Resolve<IBarcodeScannerFactory>(), container.Resolve<GlobalMessageStore>()));
            container.AddInstance(new ShutdownStore());
            container.AddInstance(new ProductStore(
                container.Resolve<GlobalMessageStore>(),
                container.Resolve<ScaleStore>(), container.Resolve<BarcodeScannerStore>(),
                container.Resolve<IStockobjectbundleService>(), container.Resolve<IColorService>(),
                container.Resolve<ApplicationSetupStore>(), container.Resolve<IBarcodeScannerService>()));
            container.AddInstance(new ConnectionStringStore(
                container.Resolve<ICustomSetupComponents>(), container.Resolve<GlobalMessageStore>(),
                container.Resolve<ModelContextFactory>()));
            container.AddInstance(new ProcessFlowStore(
                container.Resolve<GlobalMessageStore>(), container.Resolve<ProductStore>(),
                container.Resolve<ScaleStore>(), container.Resolve<IMeasurementService>(),
                container.Resolve<IColorService>()));
            // the navigation, globalMessage and modalNavigation stores are used by the home-screen view model and
            // are registered by export functionality only to avoid different instance registrations

            return container;
        }
    }
}