﻿using Mt.Proxaml.Wpf.Services;
using Mt.Proxaml.Wpf.Stores;
using Mt.Proxaml.Wpf.ViewModels;
using MT.Singularity.Composition;

namespace Mt.Proxaml.Wpf.CompositionContainerExtensions
{
    public static class AddViewModelsToCompositionContainerExtension
    {
        public static CompositionContainer AddViewModelsToCompositionContainer(this CompositionContainer container)
        {
            container.AddFactory(() => CreateFirstViewModel(container));
            container.AddFactory(() => CreateModalViewModel(container));
            container.AddInstance(CreateGlobalMessageViewModel(container));
            container.AddInstance(CreateDataBankStViewModel(container));

            return container;
        }

        private static ModalViewModel CreateModalViewModel(IObjectResolver resolver)
        {
            return new ModalViewModel(resolver.Resolve<CloseModalNavigationService>());
        }

        private static FirstViewModel CreateFirstViewModel(IObjectResolver resolver)
        {
            return new FirstViewModel(
                resolver.Resolve<RibbonItemStore>(),
                resolver.Resolve<ProcessFlowStore>(), resolver.Resolve<ProductStore>(),
                resolver.Resolve<BarcodeScannerStore>(), resolver.Resolve<ShutdownStore>(),
                resolver.Resolve<UserStore>(), resolver.Resolve<GlobalMessageStore>(),
                resolver.Resolve<ApplicationSetupStore>());
        }

        private static GlobalMessageViewModel CreateGlobalMessageViewModel(IObjectResolver resolver)
        {
            return new GlobalMessageViewModel(resolver.Resolve<GlobalMessageStore>());
        }

        private static DatabankStatusViewModel CreateDataBankStViewModel(IObjectResolver resolver)
        {
            return new DatabankStatusViewModel(
                resolver.Resolve<ConnectionStringStore>(),
                resolver.Resolve<GlobalMessageStore>());
        }
    }
}