﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Mt.Proxaml.Application.Services;
using Mt.Proxaml.persistence.Context;

namespace Mt.Proxaml.Wpf.CompositionContainerExtensions
{
    public static class AddDbContextToCompositionContainerExtension
    {
        public static IHostBuilder AddDbContext(this IHostBuilder host)
        {
            host.ConfigureServices((context, services) =>
            {
                string connectionString = SerializationService.GetConnectionString();

                ModelContextFactory modelContextFactory = new ModelContextFactory();
                modelContextFactory.ConfigureDbContext = o => o.UseOracle(connectionString);
                services.AddSingleton(modelContextFactory);
            });

            return host;
        }
    }
}