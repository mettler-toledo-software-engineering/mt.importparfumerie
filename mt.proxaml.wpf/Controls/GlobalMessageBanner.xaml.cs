﻿using System.Windows.Controls;

namespace Mt.Proxaml.Wpf.Controls
{
    /// <summary>
    ///     Interaction logic for GlobalMessageBanner.xaml
    /// </summary>
    public partial class GlobalMessageBanner : UserControl
    {
        public GlobalMessageBanner()
        {
            InitializeComponent();
        }
    }
}