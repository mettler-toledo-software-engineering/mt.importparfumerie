﻿using System;
using System.Threading.Tasks;

namespace Mt.Proxaml.Scanner
{
    public interface IScanner
    {
        event Action<string> BarcodeDataReceived;
        void OpenPort();
        Task<bool> ClosePortAsync();
    }
}