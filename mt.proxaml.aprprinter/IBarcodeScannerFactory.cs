﻿using System.Threading.Tasks;

namespace Mt.Proxaml.Scanner
{
    public interface IBarcodeScannerFactory
    {
        Task<IScanner> CreateScannerAsync(int barcodeScannerPort);
    }
}