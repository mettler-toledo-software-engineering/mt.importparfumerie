﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using MT.Singularity.IO;
using MT.Singularity.Serialization;

namespace Mt.Proxaml.Scanner
{
    public class Scanner : IScanner
    {
        private readonly StringSerializer _stringSerializer;

        public Scanner(StringSerializer stringSerializer)
        {
            _stringSerializer = stringSerializer;
        }

        public event Action<string> BarcodeDataReceived;

        public void OpenPort()
        {
            _stringSerializer?.Open();
            var eventEmitter = new ChannelEventEmitter<string>(_stringSerializer);
            eventEmitter.MessageRead += EventEmitterOnMessageRead;
        }

        // port is closed asynchronously, because synchronous closing led to freezing of the app 
        public async Task<bool> ClosePortAsync()
        {
            try
            {
                await _stringSerializer.CloseAsync();
                _stringSerializer.Dispose();
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }
        }

        private void EventEmitterOnMessageRead(object sender, MessageReadEventArgs<string> e)
        {
            string receivedBarcode = e.Message;
            if (string.IsNullOrEmpty(receivedBarcode))
            {
                return;
            }

            BarcodeDataReceived?.Invoke(receivedBarcode);
        }
    }
}