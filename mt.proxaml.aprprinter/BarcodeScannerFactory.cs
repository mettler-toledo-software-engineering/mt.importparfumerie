﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using MT.Singularity.IO;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Serialization;

namespace Mt.Proxaml.Scanner
{
    public class BarcodeScannerFactory : IBarcodeScannerFactory
    {
        private readonly IInterfaceService _interfaceService;

        public BarcodeScannerFactory(IInterfaceService interfaceService)
        {
            _interfaceService = interfaceService;
        }

        public async Task<IScanner> CreateScannerAsync(int barcodeScannerPort)
        {
            StringSerializer stringSerializer = await SetupConnectionAsync(barcodeScannerPort);
            return new Scanner(stringSerializer);
        }

        private async Task<StringSerializer> SetupConnectionAsync(int barcodeScannerPort)
        {
            try
            {
                var serialInterfaces = await _interfaceService.GetAllInterfacesOfTypeAsync<ISerialInterface>();
                ISerialInterface serialInterface =
                    serialInterfaces.FirstOrDefault(serial => serial.LogicalPort == barcodeScannerPort);
                if (serialInterface != null)
                {
                    var actualConnectionChannel =
                        await serialInterface.CreateConnectionChannelAsync();
                    DelimiterSerializer delimiterSerializer = new DelimiterSerializer(actualConnectionChannel, CommonDataSegments.Cr);
                    return new StringSerializer(delimiterSerializer);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return null;
        }
    }
}